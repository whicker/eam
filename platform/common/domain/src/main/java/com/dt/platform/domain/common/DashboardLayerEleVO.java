package com.dt.platform.domain.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import com.github.foxnic.api.model.CompositeParameter;
import javax.persistence.Transient;
import com.github.foxnic.commons.bean.BeanUtil;
import com.github.foxnic.dao.entity.EntityContext;
import com.github.foxnic.dao.entity.Entity;
import java.util.Map;
import com.dt.platform.domain.common.meta.DashboardLayerEleVOMeta;
import com.github.foxnic.commons.lang.DataParser;
import java.util.Date;
import com.github.foxnic.sql.data.ExprRcd;



/**
 * 组件VO类型
 * <p>组件 , 数据表 sys_dashboard_layer_ele 的通用VO类型</p>
 * @author 金杰 , maillank@qq.com
 * @since 2023-10-24 07:43:40
 * @sign AB6FDE4806C52598E59FAE7D52E959F9
 * 此文件由工具自动生成，请勿修改。若表结构或配置发生变动，请使用工具重新生成。
*/

@ApiModel(description = "组件VO类型 ; 组件 , 数据表 sys_dashboard_layer_ele 的通用VO类型" , parent = DashboardLayerEle.class)
public class DashboardLayerEleVO extends DashboardLayerEle {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 页码
	*/
	@ApiModelProperty(required = false,value="页码" , notes = "")
	private Integer pageIndex;
	
	/**
	 * 分页大小
	*/
	@ApiModelProperty(required = false,value="分页大小" , notes = "")
	private Integer pageSize;
	
	/**
	 * 搜索字段
	*/
	@ApiModelProperty(required = false,value="搜索字段" , notes = "")
	private String searchField;
	
	/**
	 * 模糊搜索字段
	*/
	@ApiModelProperty(required = false,value="模糊搜索字段" , notes = "")
	private String fuzzyField;
	
	/**
	 * 搜索的值
	*/
	@ApiModelProperty(required = false,value="搜索的值" , notes = "")
	private String searchValue;
	
	/**
	 * 已修改字段
	*/
	@ApiModelProperty(required = false,value="已修改字段" , notes = "")
	private List<String> dirtyFields;
	
	/**
	 * 排序字段
	*/
	@ApiModelProperty(required = false,value="排序字段" , notes = "")
	private String sortField;
	
	/**
	 * 排序方式
	*/
	@ApiModelProperty(required = false,value="排序方式" , notes = "")
	private String sortType;
	
	/**
	 * 数据来源：前端指定不同的来源，后端可按来源执行不同的逻辑
	*/
	@ApiModelProperty(required = false,value="数据来源" , notes = "前端指定不同的来源，后端可按来源执行不同的逻辑")
	private String dataOrigin;
	
	/**
	 * 查询逻辑：默认and，可指定 or 
	*/
	@ApiModelProperty(required = false,value="查询逻辑" , notes = "默认and，可指定 or ")
	private String queryLogic;
	
	/**
	 * 请求动作：前端指定不同的Action，后端可Action执行不同的逻辑
	*/
	@ApiModelProperty(required = false,value="请求动作" , notes = "前端指定不同的Action，后端可Action执行不同的逻辑")
	private String requestAction;
	
	/**
	 * 主键清单：用于接收批量主键参数
	*/
	@ApiModelProperty(required = false,value="主键清单" , notes = "用于接收批量主键参数")
	private List<String> ids;
	
	/**
	 * 获得 页码<br>
	 * @return 页码
	*/
	public Integer getPageIndex() {
		return pageIndex;
	}
	
	/**
	 * 设置 页码
	 * @param pageIndex 页码
	 * @return 当前对象
	*/
	public DashboardLayerEleVO setPageIndex(Integer pageIndex) {
		this.pageIndex=pageIndex;
		return this;
	}
	
	/**
	 * 获得 分页大小<br>
	 * @return 分页大小
	*/
	public Integer getPageSize() {
		return pageSize;
	}
	
	/**
	 * 设置 分页大小
	 * @param pageSize 分页大小
	 * @return 当前对象
	*/
	public DashboardLayerEleVO setPageSize(Integer pageSize) {
		this.pageSize=pageSize;
		return this;
	}
	
	/**
	 * 获得 搜索字段<br>
	 * @return 搜索字段
	*/
	public String getSearchField() {
		return searchField;
	}
	
	/**
	 * 设置 搜索字段
	 * @param searchField 搜索字段
	 * @return 当前对象
	*/
	public DashboardLayerEleVO setSearchField(String searchField) {
		this.searchField=searchField;
		return this;
	}
	
	/**
	 * 获得 模糊搜索字段<br>
	 * @return 模糊搜索字段
	*/
	public String getFuzzyField() {
		return fuzzyField;
	}
	
	/**
	 * 设置 模糊搜索字段
	 * @param fuzzyField 模糊搜索字段
	 * @return 当前对象
	*/
	public DashboardLayerEleVO setFuzzyField(String fuzzyField) {
		this.fuzzyField=fuzzyField;
		return this;
	}
	
	/**
	 * 获得 搜索的值<br>
	 * @return 搜索的值
	*/
	public String getSearchValue() {
		return searchValue;
	}
	
	/**
	 * 设置 搜索的值
	 * @param searchValue 搜索的值
	 * @return 当前对象
	*/
	public DashboardLayerEleVO setSearchValue(String searchValue) {
		this.searchValue=searchValue;
		return this;
	}
	
	/**
	 * 获得 已修改字段<br>
	 * @return 已修改字段
	*/
	public List<String> getDirtyFields() {
		return dirtyFields;
	}
	
	/**
	 * 设置 已修改字段
	 * @param dirtyFields 已修改字段
	 * @return 当前对象
	*/
	public DashboardLayerEleVO setDirtyFields(List<String> dirtyFields) {
		this.dirtyFields=dirtyFields;
		return this;
	}
	
	/**
	 * 添加 已修改字段
	 * @param dirtyField 已修改字段
	 * @return 当前对象
	*/
	public DashboardLayerEleVO addDirtyField(String... dirtyField) {
		if(this.dirtyFields==null) dirtyFields=new ArrayList<>();
		this.dirtyFields.addAll(Arrays.asList(dirtyField));
		return this;
	}
	
	/**
	 * 获得 排序字段<br>
	 * @return 排序字段
	*/
	public String getSortField() {
		return sortField;
	}
	
	/**
	 * 设置 排序字段
	 * @param sortField 排序字段
	 * @return 当前对象
	*/
	public DashboardLayerEleVO setSortField(String sortField) {
		this.sortField=sortField;
		return this;
	}
	
	/**
	 * 获得 排序方式<br>
	 * @return 排序方式
	*/
	public String getSortType() {
		return sortType;
	}
	
	/**
	 * 设置 排序方式
	 * @param sortType 排序方式
	 * @return 当前对象
	*/
	public DashboardLayerEleVO setSortType(String sortType) {
		this.sortType=sortType;
		return this;
	}
	
	/**
	 * 获得 数据来源<br>
	 * 前端指定不同的来源，后端可按来源执行不同的逻辑
	 * @return 数据来源
	*/
	public String getDataOrigin() {
		return dataOrigin;
	}
	
	/**
	 * 设置 数据来源
	 * @param dataOrigin 数据来源
	 * @return 当前对象
	*/
	public DashboardLayerEleVO setDataOrigin(String dataOrigin) {
		this.dataOrigin=dataOrigin;
		return this;
	}
	
	/**
	 * 获得 查询逻辑<br>
	 * 默认and，可指定 or 
	 * @return 查询逻辑
	*/
	public String getQueryLogic() {
		return queryLogic;
	}
	
	/**
	 * 设置 查询逻辑
	 * @param queryLogic 查询逻辑
	 * @return 当前对象
	*/
	public DashboardLayerEleVO setQueryLogic(String queryLogic) {
		this.queryLogic=queryLogic;
		return this;
	}
	
	/**
	 * 获得 请求动作<br>
	 * 前端指定不同的Action，后端可Action执行不同的逻辑
	 * @return 请求动作
	*/
	public String getRequestAction() {
		return requestAction;
	}
	
	/**
	 * 设置 请求动作
	 * @param requestAction 请求动作
	 * @return 当前对象
	*/
	public DashboardLayerEleVO setRequestAction(String requestAction) {
		this.requestAction=requestAction;
		return this;
	}
	
	/**
	 * 获得 主键清单<br>
	 * 用于接收批量主键参数
	 * @return 主键清单
	*/
	public List<String> getIds() {
		return ids;
	}
	
	/**
	 * 设置 主键清单
	 * @param ids 主键清单
	 * @return 当前对象
	*/
	public DashboardLayerEleVO setIds(List<String> ids) {
		this.ids=ids;
		return this;
	}
	
	/**
	 * 添加 主键清单
	 * @param id 主键清单
	 * @return 当前对象
	*/
	public DashboardLayerEleVO addId(String... id) {
		if(this.ids==null) ids=new ArrayList<>();
		this.ids.addAll(Arrays.asList(id));
		return this;
	}
	@Transient
	private transient CompositeParameter $compositeParameter;
	/**
	 * 获得解析后的复合查询参数
	 */
	@Transient
	public CompositeParameter getCompositeParameter() {
		if($compositeParameter!=null) return  $compositeParameter;
		$compositeParameter=new CompositeParameter(this.getSearchValue(),BeanUtil.toMap(this));
		return  $compositeParameter;
	}

	/**
	 * 将自己转换成指定类型的PO
	 * @param poType  PO类型
	 * @return DashboardLayerEleVO , 转换好的 DashboardLayerEleVO 对象
	*/
	@Transient
	public <T extends Entity> T toPO(Class<T> poType) {
		return EntityContext.create(poType, this);
	}

	/**
	 * 将自己转换成任意指定类型
	 * @param pojoType  Pojo类型
	 * @return DashboardLayerEleVO , 转换好的 PoJo 对象
	*/
	@Transient
	public <T> T toPojo(Class<T> pojoType) {
		if(Entity.class.isAssignableFrom(pojoType)) {
			return (T)this.toPO((Class<Entity>)pojoType);
		}
		try {
			T pojo=pojoType.newInstance();
			EntityContext.copyProperties(pojo, this);
			return pojo;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 克隆当前对象
	*/
	@Transient
	public DashboardLayerEleVO clone() {
		return duplicate(true);
	}

	/**
	 * 复制当前对象
	 * @param all 是否复制全部属性，当 false 时，仅复制来自数据表的属性
	*/
	@Transient
	public DashboardLayerEleVO duplicate(boolean all) {
		com.dt.platform.domain.common.meta.DashboardLayerEleVOMeta.$$proxy$$ inst = new com.dt.platform.domain.common.meta.DashboardLayerEleVOMeta.$$proxy$$();
		inst.setNotes(this.getNotes());
		inst.setUpdateTime(this.getUpdateTime());
		inst.setSource(this.getSource());
		inst.setSort(this.getSort());
		inst.setTitle(this.getTitle());
		inst.setType(this.getType());
		inst.setVersion(this.getVersion());
		inst.setCreateBy(this.getCreateBy());
		inst.setLayerId(this.getLayerId());
		inst.setDeleted(this.getDeleted());
		inst.setDashboardId(this.getDashboardId());
		inst.setCreateTime(this.getCreateTime());
		inst.setUpdateBy(this.getUpdateBy());
		inst.setDeleteTime(this.getDeleteTime());
		inst.setJson(this.getJson());
		inst.setDeleteBy(this.getDeleteBy());
		inst.setId(this.getId());
		inst.setStatus(this.getStatus());
		if(all) {
			inst.setSearchField(this.getSearchField());
			inst.setRequestAction(this.getRequestAction());
			inst.setFuzzyField(this.getFuzzyField());
			inst.setPageSize(this.getPageSize());
			inst.setPageIndex(this.getPageIndex());
			inst.setSortType(this.getSortType());
			inst.setReportChart(this.getReportChart());
			inst.setDirtyFields(this.getDirtyFields());
			inst.setSortField(this.getSortField());
			inst.setDataOrigin(this.getDataOrigin());
			inst.setIds(this.getIds());
			inst.setQueryLogic(this.getQueryLogic());
			inst.setDashboardLayer(this.getDashboardLayer());
			inst.setSearchValue(this.getSearchValue());
		}
		inst.clearModifies();
		return inst;
	}

	/**
	 * 克隆当前对象
	*/
	@Transient
	public DashboardLayerEleVO clone(boolean deep) {
		return EntityContext.clone(DashboardLayerEleVO.class,this,deep);
	}

	/**
	 * 将 Map 转换成 DashboardLayerEleVO
	 * @param dashboardLayerEleMap 包含实体信息的 Map 对象
	 * @return DashboardLayerEleVO , 转换好的的 DashboardLayerEle 对象
	*/
	@Transient
	public static DashboardLayerEleVO createFrom(Map<String,Object> dashboardLayerEleMap) {
		if(dashboardLayerEleMap==null) return null;
		DashboardLayerEleVO vo = create();
		EntityContext.copyProperties(vo,dashboardLayerEleMap);
		vo.clearModifies();
		return vo;
	}

	/**
	 * 将 Pojo 转换成 DashboardLayerEleVO
	 * @param pojo 包含实体信息的 Pojo 对象
	 * @return DashboardLayerEleVO , 转换好的的 DashboardLayerEle 对象
	*/
	@Transient
	public static DashboardLayerEleVO createFrom(Object pojo) {
		if(pojo==null) return null;
		DashboardLayerEleVO vo = create();
		EntityContext.copyProperties(vo,pojo);
		vo.clearModifies();
		return vo;
	}

	/**
	 * 创建一个 DashboardLayerEleVO，等同于 new
	 * @return DashboardLayerEleVO 对象
	*/
	@Transient
	public static DashboardLayerEleVO create() {
		return new com.dt.platform.domain.common.meta.DashboardLayerEleVOMeta.$$proxy$$();
	}

	/**
	 * 从 Map 读取
	 * @param map 记录数据
	 * @param cast 是否用 DataParser 进行类型转换
	 * @return  是否读取成功
	*/
	public boolean read(Map<String, Object> map,boolean cast) {
		if(map==null) return false;
		if(cast) {
			this.setNotes(DataParser.parse(String.class, map.get(DashboardLayerEleVOMeta.NOTES)));
			this.setUpdateTime(DataParser.parse(Date.class, map.get(DashboardLayerEleVOMeta.UPDATE_TIME)));
			this.setSource(DataParser.parse(String.class, map.get(DashboardLayerEleVOMeta.SOURCE)));
			this.setSort(DataParser.parse(Integer.class, map.get(DashboardLayerEleVOMeta.SORT)));
			this.setTitle(DataParser.parse(String.class, map.get(DashboardLayerEleVOMeta.TITLE)));
			this.setType(DataParser.parse(String.class, map.get(DashboardLayerEleVOMeta.TYPE)));
			this.setVersion(DataParser.parse(Integer.class, map.get(DashboardLayerEleVOMeta.VERSION)));
			this.setCreateBy(DataParser.parse(String.class, map.get(DashboardLayerEleVOMeta.CREATE_BY)));
			this.setLayerId(DataParser.parse(String.class, map.get(DashboardLayerEleVOMeta.LAYER_ID)));
			this.setDeleted(DataParser.parse(Integer.class, map.get(DashboardLayerEleVOMeta.DELETED)));
			this.setDashboardId(DataParser.parse(String.class, map.get(DashboardLayerEleVOMeta.DASHBOARD_ID)));
			this.setCreateTime(DataParser.parse(Date.class, map.get(DashboardLayerEleVOMeta.CREATE_TIME)));
			this.setUpdateBy(DataParser.parse(String.class, map.get(DashboardLayerEleVOMeta.UPDATE_BY)));
			this.setDeleteTime(DataParser.parse(Date.class, map.get(DashboardLayerEleVOMeta.DELETE_TIME)));
			this.setJson(DataParser.parse(String.class, map.get(DashboardLayerEleVOMeta.JSON)));
			this.setDeleteBy(DataParser.parse(String.class, map.get(DashboardLayerEleVOMeta.DELETE_BY)));
			this.setId(DataParser.parse(String.class, map.get(DashboardLayerEleVOMeta.ID)));
			this.setStatus(DataParser.parse(String.class, map.get(DashboardLayerEleVOMeta.STATUS)));
			// others
			this.setSearchField(DataParser.parse(String.class, map.get(DashboardLayerEleVOMeta.SEARCH_FIELD)));
			this.setRequestAction(DataParser.parse(String.class, map.get(DashboardLayerEleVOMeta.REQUEST_ACTION)));
			this.setFuzzyField(DataParser.parse(String.class, map.get(DashboardLayerEleVOMeta.FUZZY_FIELD)));
			this.setPageSize(DataParser.parse(Integer.class, map.get(DashboardLayerEleVOMeta.PAGE_SIZE)));
			this.setPageIndex(DataParser.parse(Integer.class, map.get(DashboardLayerEleVOMeta.PAGE_INDEX)));
			this.setSortType(DataParser.parse(String.class, map.get(DashboardLayerEleVOMeta.SORT_TYPE)));
			this.setReportChart(DataParser.parse(Report.class, map.get(DashboardLayerEleVOMeta.REPORT_CHART)));
			this.setSortField(DataParser.parse(String.class, map.get(DashboardLayerEleVOMeta.SORT_FIELD)));
			this.setDataOrigin(DataParser.parse(String.class, map.get(DashboardLayerEleVOMeta.DATA_ORIGIN)));
			this.setQueryLogic(DataParser.parse(String.class, map.get(DashboardLayerEleVOMeta.QUERY_LOGIC)));
			this.setDashboardLayer(DataParser.parse(DashboardLayer.class, map.get(DashboardLayerEleVOMeta.DASHBOARD_LAYER)));
			this.setSearchValue(DataParser.parse(String.class, map.get(DashboardLayerEleVOMeta.SEARCH_VALUE)));
			return true;
		} else {
			try {
				this.setNotes( (String)map.get(DashboardLayerEleVOMeta.NOTES));
				this.setUpdateTime( (Date)map.get(DashboardLayerEleVOMeta.UPDATE_TIME));
				this.setSource( (String)map.get(DashboardLayerEleVOMeta.SOURCE));
				this.setSort( (Integer)map.get(DashboardLayerEleVOMeta.SORT));
				this.setTitle( (String)map.get(DashboardLayerEleVOMeta.TITLE));
				this.setType( (String)map.get(DashboardLayerEleVOMeta.TYPE));
				this.setVersion( (Integer)map.get(DashboardLayerEleVOMeta.VERSION));
				this.setCreateBy( (String)map.get(DashboardLayerEleVOMeta.CREATE_BY));
				this.setLayerId( (String)map.get(DashboardLayerEleVOMeta.LAYER_ID));
				this.setDeleted( (Integer)map.get(DashboardLayerEleVOMeta.DELETED));
				this.setDashboardId( (String)map.get(DashboardLayerEleVOMeta.DASHBOARD_ID));
				this.setCreateTime( (Date)map.get(DashboardLayerEleVOMeta.CREATE_TIME));
				this.setUpdateBy( (String)map.get(DashboardLayerEleVOMeta.UPDATE_BY));
				this.setDeleteTime( (Date)map.get(DashboardLayerEleVOMeta.DELETE_TIME));
				this.setJson( (String)map.get(DashboardLayerEleVOMeta.JSON));
				this.setDeleteBy( (String)map.get(DashboardLayerEleVOMeta.DELETE_BY));
				this.setId( (String)map.get(DashboardLayerEleVOMeta.ID));
				this.setStatus( (String)map.get(DashboardLayerEleVOMeta.STATUS));
				// others
				this.setSearchField( (String)map.get(DashboardLayerEleVOMeta.SEARCH_FIELD));
				this.setRequestAction( (String)map.get(DashboardLayerEleVOMeta.REQUEST_ACTION));
				this.setFuzzyField( (String)map.get(DashboardLayerEleVOMeta.FUZZY_FIELD));
				this.setPageSize( (Integer)map.get(DashboardLayerEleVOMeta.PAGE_SIZE));
				this.setPageIndex( (Integer)map.get(DashboardLayerEleVOMeta.PAGE_INDEX));
				this.setSortType( (String)map.get(DashboardLayerEleVOMeta.SORT_TYPE));
				this.setReportChart( (Report)map.get(DashboardLayerEleVOMeta.REPORT_CHART));
				this.setSortField( (String)map.get(DashboardLayerEleVOMeta.SORT_FIELD));
				this.setDataOrigin( (String)map.get(DashboardLayerEleVOMeta.DATA_ORIGIN));
				this.setQueryLogic( (String)map.get(DashboardLayerEleVOMeta.QUERY_LOGIC));
				this.setDashboardLayer( (DashboardLayer)map.get(DashboardLayerEleVOMeta.DASHBOARD_LAYER));
				this.setSearchValue( (String)map.get(DashboardLayerEleVOMeta.SEARCH_VALUE));
				return true;
			} catch (Exception e) {
				return false;
			}
		}
	}

	/**
	 * 从 Map 读取
	 * @param r 记录数据
	 * @param cast 是否用 DataParser 进行类型转换
	 * @return  是否读取成功
	*/
	public boolean read(ExprRcd r,boolean cast) {
		if(r==null) return false;
		if(cast) {
			this.setNotes(DataParser.parse(String.class, r.getValue(DashboardLayerEleVOMeta.NOTES)));
			this.setUpdateTime(DataParser.parse(Date.class, r.getValue(DashboardLayerEleVOMeta.UPDATE_TIME)));
			this.setSource(DataParser.parse(String.class, r.getValue(DashboardLayerEleVOMeta.SOURCE)));
			this.setSort(DataParser.parse(Integer.class, r.getValue(DashboardLayerEleVOMeta.SORT)));
			this.setTitle(DataParser.parse(String.class, r.getValue(DashboardLayerEleVOMeta.TITLE)));
			this.setType(DataParser.parse(String.class, r.getValue(DashboardLayerEleVOMeta.TYPE)));
			this.setVersion(DataParser.parse(Integer.class, r.getValue(DashboardLayerEleVOMeta.VERSION)));
			this.setCreateBy(DataParser.parse(String.class, r.getValue(DashboardLayerEleVOMeta.CREATE_BY)));
			this.setLayerId(DataParser.parse(String.class, r.getValue(DashboardLayerEleVOMeta.LAYER_ID)));
			this.setDeleted(DataParser.parse(Integer.class, r.getValue(DashboardLayerEleVOMeta.DELETED)));
			this.setDashboardId(DataParser.parse(String.class, r.getValue(DashboardLayerEleVOMeta.DASHBOARD_ID)));
			this.setCreateTime(DataParser.parse(Date.class, r.getValue(DashboardLayerEleVOMeta.CREATE_TIME)));
			this.setUpdateBy(DataParser.parse(String.class, r.getValue(DashboardLayerEleVOMeta.UPDATE_BY)));
			this.setDeleteTime(DataParser.parse(Date.class, r.getValue(DashboardLayerEleVOMeta.DELETE_TIME)));
			this.setJson(DataParser.parse(String.class, r.getValue(DashboardLayerEleVOMeta.JSON)));
			this.setDeleteBy(DataParser.parse(String.class, r.getValue(DashboardLayerEleVOMeta.DELETE_BY)));
			this.setId(DataParser.parse(String.class, r.getValue(DashboardLayerEleVOMeta.ID)));
			this.setStatus(DataParser.parse(String.class, r.getValue(DashboardLayerEleVOMeta.STATUS)));
			return true;
		} else {
			try {
				this.setNotes( (String)r.getValue(DashboardLayerEleVOMeta.NOTES));
				this.setUpdateTime( (Date)r.getValue(DashboardLayerEleVOMeta.UPDATE_TIME));
				this.setSource( (String)r.getValue(DashboardLayerEleVOMeta.SOURCE));
				this.setSort( (Integer)r.getValue(DashboardLayerEleVOMeta.SORT));
				this.setTitle( (String)r.getValue(DashboardLayerEleVOMeta.TITLE));
				this.setType( (String)r.getValue(DashboardLayerEleVOMeta.TYPE));
				this.setVersion( (Integer)r.getValue(DashboardLayerEleVOMeta.VERSION));
				this.setCreateBy( (String)r.getValue(DashboardLayerEleVOMeta.CREATE_BY));
				this.setLayerId( (String)r.getValue(DashboardLayerEleVOMeta.LAYER_ID));
				this.setDeleted( (Integer)r.getValue(DashboardLayerEleVOMeta.DELETED));
				this.setDashboardId( (String)r.getValue(DashboardLayerEleVOMeta.DASHBOARD_ID));
				this.setCreateTime( (Date)r.getValue(DashboardLayerEleVOMeta.CREATE_TIME));
				this.setUpdateBy( (String)r.getValue(DashboardLayerEleVOMeta.UPDATE_BY));
				this.setDeleteTime( (Date)r.getValue(DashboardLayerEleVOMeta.DELETE_TIME));
				this.setJson( (String)r.getValue(DashboardLayerEleVOMeta.JSON));
				this.setDeleteBy( (String)r.getValue(DashboardLayerEleVOMeta.DELETE_BY));
				this.setId( (String)r.getValue(DashboardLayerEleVOMeta.ID));
				this.setStatus( (String)r.getValue(DashboardLayerEleVOMeta.STATUS));
				return true;
			} catch (Exception e) {
				return false;
			}
		}
	}
}