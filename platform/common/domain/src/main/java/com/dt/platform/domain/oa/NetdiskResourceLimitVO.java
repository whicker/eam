package com.dt.platform.domain.oa;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import com.github.foxnic.api.model.CompositeParameter;
import javax.persistence.Transient;
import com.github.foxnic.commons.bean.BeanUtil;
import com.github.foxnic.dao.entity.EntityContext;
import com.github.foxnic.dao.entity.Entity;
import java.util.Map;
import com.dt.platform.domain.oa.meta.NetdiskResourceLimitVOMeta;
import com.github.foxnic.commons.lang.DataParser;
import java.util.Date;
import org.github.foxnic.web.domain.hrm.Employee;
import com.github.foxnic.sql.data.ExprRcd;



/**
 * 资源限制VO类型
 * <p>资源限制 , 数据表 oa_netdisk_resource_limit 的通用VO类型</p>
 * @author 金杰 , maillank@qq.com
 * @since 2023-09-27 22:40:34
 * @sign B95C5A3A9A14AD0EDA7C0457A65F1F83
 * 此文件由工具自动生成，请勿修改。若表结构或配置发生变动，请使用工具重新生成。
*/

@ApiModel(description = "资源限制VO类型 ; 资源限制 , 数据表 oa_netdisk_resource_limit 的通用VO类型" , parent = NetdiskResourceLimit.class)
public class NetdiskResourceLimitVO extends NetdiskResourceLimit {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 页码
	*/
	@ApiModelProperty(required = false,value="页码" , notes = "")
	private Integer pageIndex;
	
	/**
	 * 分页大小
	*/
	@ApiModelProperty(required = false,value="分页大小" , notes = "")
	private Integer pageSize;
	
	/**
	 * 搜索字段
	*/
	@ApiModelProperty(required = false,value="搜索字段" , notes = "")
	private String searchField;
	
	/**
	 * 模糊搜索字段
	*/
	@ApiModelProperty(required = false,value="模糊搜索字段" , notes = "")
	private String fuzzyField;
	
	/**
	 * 搜索的值
	*/
	@ApiModelProperty(required = false,value="搜索的值" , notes = "")
	private String searchValue;
	
	/**
	 * 已修改字段
	*/
	@ApiModelProperty(required = false,value="已修改字段" , notes = "")
	private List<String> dirtyFields;
	
	/**
	 * 排序字段
	*/
	@ApiModelProperty(required = false,value="排序字段" , notes = "")
	private String sortField;
	
	/**
	 * 排序方式
	*/
	@ApiModelProperty(required = false,value="排序方式" , notes = "")
	private String sortType;
	
	/**
	 * 数据来源：前端指定不同的来源，后端可按来源执行不同的逻辑
	*/
	@ApiModelProperty(required = false,value="数据来源" , notes = "前端指定不同的来源，后端可按来源执行不同的逻辑")
	private String dataOrigin;
	
	/**
	 * 查询逻辑：默认and，可指定 or 
	*/
	@ApiModelProperty(required = false,value="查询逻辑" , notes = "默认and，可指定 or ")
	private String queryLogic;
	
	/**
	 * 请求动作：前端指定不同的Action，后端可Action执行不同的逻辑
	*/
	@ApiModelProperty(required = false,value="请求动作" , notes = "前端指定不同的Action，后端可Action执行不同的逻辑")
	private String requestAction;
	
	/**
	 * 主键清单：用于接收批量主键参数
	*/
	@ApiModelProperty(required = false,value="主键清单" , notes = "用于接收批量主键参数")
	private List<String> ids;
	
	/**
	 * 获得 页码<br>
	 * @return 页码
	*/
	public Integer getPageIndex() {
		return pageIndex;
	}
	
	/**
	 * 设置 页码
	 * @param pageIndex 页码
	 * @return 当前对象
	*/
	public NetdiskResourceLimitVO setPageIndex(Integer pageIndex) {
		this.pageIndex=pageIndex;
		return this;
	}
	
	/**
	 * 获得 分页大小<br>
	 * @return 分页大小
	*/
	public Integer getPageSize() {
		return pageSize;
	}
	
	/**
	 * 设置 分页大小
	 * @param pageSize 分页大小
	 * @return 当前对象
	*/
	public NetdiskResourceLimitVO setPageSize(Integer pageSize) {
		this.pageSize=pageSize;
		return this;
	}
	
	/**
	 * 获得 搜索字段<br>
	 * @return 搜索字段
	*/
	public String getSearchField() {
		return searchField;
	}
	
	/**
	 * 设置 搜索字段
	 * @param searchField 搜索字段
	 * @return 当前对象
	*/
	public NetdiskResourceLimitVO setSearchField(String searchField) {
		this.searchField=searchField;
		return this;
	}
	
	/**
	 * 获得 模糊搜索字段<br>
	 * @return 模糊搜索字段
	*/
	public String getFuzzyField() {
		return fuzzyField;
	}
	
	/**
	 * 设置 模糊搜索字段
	 * @param fuzzyField 模糊搜索字段
	 * @return 当前对象
	*/
	public NetdiskResourceLimitVO setFuzzyField(String fuzzyField) {
		this.fuzzyField=fuzzyField;
		return this;
	}
	
	/**
	 * 获得 搜索的值<br>
	 * @return 搜索的值
	*/
	public String getSearchValue() {
		return searchValue;
	}
	
	/**
	 * 设置 搜索的值
	 * @param searchValue 搜索的值
	 * @return 当前对象
	*/
	public NetdiskResourceLimitVO setSearchValue(String searchValue) {
		this.searchValue=searchValue;
		return this;
	}
	
	/**
	 * 获得 已修改字段<br>
	 * @return 已修改字段
	*/
	public List<String> getDirtyFields() {
		return dirtyFields;
	}
	
	/**
	 * 设置 已修改字段
	 * @param dirtyFields 已修改字段
	 * @return 当前对象
	*/
	public NetdiskResourceLimitVO setDirtyFields(List<String> dirtyFields) {
		this.dirtyFields=dirtyFields;
		return this;
	}
	
	/**
	 * 添加 已修改字段
	 * @param dirtyField 已修改字段
	 * @return 当前对象
	*/
	public NetdiskResourceLimitVO addDirtyField(String... dirtyField) {
		if(this.dirtyFields==null) dirtyFields=new ArrayList<>();
		this.dirtyFields.addAll(Arrays.asList(dirtyField));
		return this;
	}
	
	/**
	 * 获得 排序字段<br>
	 * @return 排序字段
	*/
	public String getSortField() {
		return sortField;
	}
	
	/**
	 * 设置 排序字段
	 * @param sortField 排序字段
	 * @return 当前对象
	*/
	public NetdiskResourceLimitVO setSortField(String sortField) {
		this.sortField=sortField;
		return this;
	}
	
	/**
	 * 获得 排序方式<br>
	 * @return 排序方式
	*/
	public String getSortType() {
		return sortType;
	}
	
	/**
	 * 设置 排序方式
	 * @param sortType 排序方式
	 * @return 当前对象
	*/
	public NetdiskResourceLimitVO setSortType(String sortType) {
		this.sortType=sortType;
		return this;
	}
	
	/**
	 * 获得 数据来源<br>
	 * 前端指定不同的来源，后端可按来源执行不同的逻辑
	 * @return 数据来源
	*/
	public String getDataOrigin() {
		return dataOrigin;
	}
	
	/**
	 * 设置 数据来源
	 * @param dataOrigin 数据来源
	 * @return 当前对象
	*/
	public NetdiskResourceLimitVO setDataOrigin(String dataOrigin) {
		this.dataOrigin=dataOrigin;
		return this;
	}
	
	/**
	 * 获得 查询逻辑<br>
	 * 默认and，可指定 or 
	 * @return 查询逻辑
	*/
	public String getQueryLogic() {
		return queryLogic;
	}
	
	/**
	 * 设置 查询逻辑
	 * @param queryLogic 查询逻辑
	 * @return 当前对象
	*/
	public NetdiskResourceLimitVO setQueryLogic(String queryLogic) {
		this.queryLogic=queryLogic;
		return this;
	}
	
	/**
	 * 获得 请求动作<br>
	 * 前端指定不同的Action，后端可Action执行不同的逻辑
	 * @return 请求动作
	*/
	public String getRequestAction() {
		return requestAction;
	}
	
	/**
	 * 设置 请求动作
	 * @param requestAction 请求动作
	 * @return 当前对象
	*/
	public NetdiskResourceLimitVO setRequestAction(String requestAction) {
		this.requestAction=requestAction;
		return this;
	}
	
	/**
	 * 获得 主键清单<br>
	 * 用于接收批量主键参数
	 * @return 主键清单
	*/
	public List<String> getIds() {
		return ids;
	}
	
	/**
	 * 设置 主键清单
	 * @param ids 主键清单
	 * @return 当前对象
	*/
	public NetdiskResourceLimitVO setIds(List<String> ids) {
		this.ids=ids;
		return this;
	}
	
	/**
	 * 添加 主键清单
	 * @param id 主键清单
	 * @return 当前对象
	*/
	public NetdiskResourceLimitVO addId(String... id) {
		if(this.ids==null) ids=new ArrayList<>();
		this.ids.addAll(Arrays.asList(id));
		return this;
	}
	@Transient
	private transient CompositeParameter $compositeParameter;
	/**
	 * 获得解析后的复合查询参数
	 */
	@Transient
	public CompositeParameter getCompositeParameter() {
		if($compositeParameter!=null) return  $compositeParameter;
		$compositeParameter=new CompositeParameter(this.getSearchValue(),BeanUtil.toMap(this));
		return  $compositeParameter;
	}

	/**
	 * 将自己转换成指定类型的PO
	 * @param poType  PO类型
	 * @return NetdiskResourceLimitVO , 转换好的 NetdiskResourceLimitVO 对象
	*/
	@Transient
	public <T extends Entity> T toPO(Class<T> poType) {
		return EntityContext.create(poType, this);
	}

	/**
	 * 将自己转换成任意指定类型
	 * @param pojoType  Pojo类型
	 * @return NetdiskResourceLimitVO , 转换好的 PoJo 对象
	*/
	@Transient
	public <T> T toPojo(Class<T> pojoType) {
		if(Entity.class.isAssignableFrom(pojoType)) {
			return (T)this.toPO((Class<Entity>)pojoType);
		}
		try {
			T pojo=pojoType.newInstance();
			EntityContext.copyProperties(pojo, this);
			return pojo;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 克隆当前对象
	*/
	@Transient
	public NetdiskResourceLimitVO clone() {
		return duplicate(true);
	}

	/**
	 * 复制当前对象
	 * @param all 是否复制全部属性，当 false 时，仅复制来自数据表的属性
	*/
	@Transient
	public NetdiskResourceLimitVO duplicate(boolean all) {
		com.dt.platform.domain.oa.meta.NetdiskResourceLimitVOMeta.$$proxy$$ inst = new com.dt.platform.domain.oa.meta.NetdiskResourceLimitVOMeta.$$proxy$$();
		inst.setPlayVideoValid(this.getPlayVideoValid());
		inst.setPlayMusicValid(this.getPlayMusicValid());
		inst.setViewPhotoValid(this.getViewPhotoValid());
		inst.setUploadFileType(this.getUploadFileType());
		inst.setUpdateTime(this.getUpdateTime());
		inst.setUserId(this.getUserId());
		inst.setCapacitySizeM(this.getCapacitySizeM());
		inst.setVersion(this.getVersion());
		inst.setCreateBy(this.getCreateBy());
		inst.setUploadMaxSizeM(this.getUploadMaxSizeM());
		inst.setDeleted(this.getDeleted());
		inst.setCreateTime(this.getCreateTime());
		inst.setUpdateBy(this.getUpdateBy());
		inst.setDeleteTime(this.getDeleteTime());
		inst.setCurrentSizeB(this.getCurrentSizeB());
		inst.setTenantId(this.getTenantId());
		inst.setDeleteBy(this.getDeleteBy());
		inst.setId(this.getId());
		inst.setViewDocValid(this.getViewDocValid());
		if(all) {
			inst.setCurrentSizeValue(this.getCurrentSizeValue());
			inst.setSearchField(this.getSearchField());
			inst.setRequestAction(this.getRequestAction());
			inst.setFuzzyField(this.getFuzzyField());
			inst.setPageSize(this.getPageSize());
			inst.setCapacitySizeValue(this.getCapacitySizeValue());
			inst.setPageIndex(this.getPageIndex());
			inst.setSortType(this.getSortType());
			inst.setDirtyFields(this.getDirtyFields());
			inst.setSortField(this.getSortField());
			inst.setDataOrigin(this.getDataOrigin());
			inst.setIds(this.getIds());
			inst.setQueryLogic(this.getQueryLogic());
			inst.setUser(this.getUser());
			inst.setSearchValue(this.getSearchValue());
		}
		inst.clearModifies();
		return inst;
	}

	/**
	 * 克隆当前对象
	*/
	@Transient
	public NetdiskResourceLimitVO clone(boolean deep) {
		return EntityContext.clone(NetdiskResourceLimitVO.class,this,deep);
	}

	/**
	 * 将 Map 转换成 NetdiskResourceLimitVO
	 * @param netdiskResourceLimitMap 包含实体信息的 Map 对象
	 * @return NetdiskResourceLimitVO , 转换好的的 NetdiskResourceLimit 对象
	*/
	@Transient
	public static NetdiskResourceLimitVO createFrom(Map<String,Object> netdiskResourceLimitMap) {
		if(netdiskResourceLimitMap==null) return null;
		NetdiskResourceLimitVO vo = create();
		EntityContext.copyProperties(vo,netdiskResourceLimitMap);
		vo.clearModifies();
		return vo;
	}

	/**
	 * 将 Pojo 转换成 NetdiskResourceLimitVO
	 * @param pojo 包含实体信息的 Pojo 对象
	 * @return NetdiskResourceLimitVO , 转换好的的 NetdiskResourceLimit 对象
	*/
	@Transient
	public static NetdiskResourceLimitVO createFrom(Object pojo) {
		if(pojo==null) return null;
		NetdiskResourceLimitVO vo = create();
		EntityContext.copyProperties(vo,pojo);
		vo.clearModifies();
		return vo;
	}

	/**
	 * 创建一个 NetdiskResourceLimitVO，等同于 new
	 * @return NetdiskResourceLimitVO 对象
	*/
	@Transient
	public static NetdiskResourceLimitVO create() {
		return new com.dt.platform.domain.oa.meta.NetdiskResourceLimitVOMeta.$$proxy$$();
	}

	/**
	 * 从 Map 读取
	 * @param map 记录数据
	 * @param cast 是否用 DataParser 进行类型转换
	 * @return  是否读取成功
	*/
	public boolean read(Map<String, Object> map,boolean cast) {
		if(map==null) return false;
		if(cast) {
			this.setPlayVideoValid(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.PLAY_VIDEO_VALID)));
			this.setPlayMusicValid(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.PLAY_MUSIC_VALID)));
			this.setViewPhotoValid(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.VIEW_PHOTO_VALID)));
			this.setUploadFileType(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.UPLOAD_FILE_TYPE)));
			this.setUpdateTime(DataParser.parse(Date.class, map.get(NetdiskResourceLimitVOMeta.UPDATE_TIME)));
			this.setUserId(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.USER_ID)));
			this.setCapacitySizeM(DataParser.parse(Long.class, map.get(NetdiskResourceLimitVOMeta.CAPACITY_SIZE_M)));
			this.setVersion(DataParser.parse(Integer.class, map.get(NetdiskResourceLimitVOMeta.VERSION)));
			this.setCreateBy(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.CREATE_BY)));
			this.setUploadMaxSizeM(DataParser.parse(Long.class, map.get(NetdiskResourceLimitVOMeta.UPLOAD_MAX_SIZE_M)));
			this.setDeleted(DataParser.parse(Integer.class, map.get(NetdiskResourceLimitVOMeta.DELETED)));
			this.setCreateTime(DataParser.parse(Date.class, map.get(NetdiskResourceLimitVOMeta.CREATE_TIME)));
			this.setUpdateBy(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.UPDATE_BY)));
			this.setDeleteTime(DataParser.parse(Date.class, map.get(NetdiskResourceLimitVOMeta.DELETE_TIME)));
			this.setCurrentSizeB(DataParser.parse(Long.class, map.get(NetdiskResourceLimitVOMeta.CURRENT_SIZE_B)));
			this.setTenantId(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.TENANT_ID)));
			this.setDeleteBy(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.DELETE_BY)));
			this.setId(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.ID)));
			this.setViewDocValid(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.VIEW_DOC_VALID)));
			// others
			this.setCurrentSizeValue(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.CURRENT_SIZE_VALUE)));
			this.setSearchField(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.SEARCH_FIELD)));
			this.setRequestAction(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.REQUEST_ACTION)));
			this.setFuzzyField(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.FUZZY_FIELD)));
			this.setPageSize(DataParser.parse(Integer.class, map.get(NetdiskResourceLimitVOMeta.PAGE_SIZE)));
			this.setCapacitySizeValue(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.CAPACITY_SIZE_VALUE)));
			this.setPageIndex(DataParser.parse(Integer.class, map.get(NetdiskResourceLimitVOMeta.PAGE_INDEX)));
			this.setSortType(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.SORT_TYPE)));
			this.setSortField(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.SORT_FIELD)));
			this.setDataOrigin(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.DATA_ORIGIN)));
			this.setQueryLogic(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.QUERY_LOGIC)));
			this.setUser(DataParser.parse(Employee.class, map.get(NetdiskResourceLimitVOMeta.USER)));
			this.setSearchValue(DataParser.parse(String.class, map.get(NetdiskResourceLimitVOMeta.SEARCH_VALUE)));
			return true;
		} else {
			try {
				this.setPlayVideoValid( (String)map.get(NetdiskResourceLimitVOMeta.PLAY_VIDEO_VALID));
				this.setPlayMusicValid( (String)map.get(NetdiskResourceLimitVOMeta.PLAY_MUSIC_VALID));
				this.setViewPhotoValid( (String)map.get(NetdiskResourceLimitVOMeta.VIEW_PHOTO_VALID));
				this.setUploadFileType( (String)map.get(NetdiskResourceLimitVOMeta.UPLOAD_FILE_TYPE));
				this.setUpdateTime( (Date)map.get(NetdiskResourceLimitVOMeta.UPDATE_TIME));
				this.setUserId( (String)map.get(NetdiskResourceLimitVOMeta.USER_ID));
				this.setCapacitySizeM( (Long)map.get(NetdiskResourceLimitVOMeta.CAPACITY_SIZE_M));
				this.setVersion( (Integer)map.get(NetdiskResourceLimitVOMeta.VERSION));
				this.setCreateBy( (String)map.get(NetdiskResourceLimitVOMeta.CREATE_BY));
				this.setUploadMaxSizeM( (Long)map.get(NetdiskResourceLimitVOMeta.UPLOAD_MAX_SIZE_M));
				this.setDeleted( (Integer)map.get(NetdiskResourceLimitVOMeta.DELETED));
				this.setCreateTime( (Date)map.get(NetdiskResourceLimitVOMeta.CREATE_TIME));
				this.setUpdateBy( (String)map.get(NetdiskResourceLimitVOMeta.UPDATE_BY));
				this.setDeleteTime( (Date)map.get(NetdiskResourceLimitVOMeta.DELETE_TIME));
				this.setCurrentSizeB( (Long)map.get(NetdiskResourceLimitVOMeta.CURRENT_SIZE_B));
				this.setTenantId( (String)map.get(NetdiskResourceLimitVOMeta.TENANT_ID));
				this.setDeleteBy( (String)map.get(NetdiskResourceLimitVOMeta.DELETE_BY));
				this.setId( (String)map.get(NetdiskResourceLimitVOMeta.ID));
				this.setViewDocValid( (String)map.get(NetdiskResourceLimitVOMeta.VIEW_DOC_VALID));
				// others
				this.setCurrentSizeValue( (String)map.get(NetdiskResourceLimitVOMeta.CURRENT_SIZE_VALUE));
				this.setSearchField( (String)map.get(NetdiskResourceLimitVOMeta.SEARCH_FIELD));
				this.setRequestAction( (String)map.get(NetdiskResourceLimitVOMeta.REQUEST_ACTION));
				this.setFuzzyField( (String)map.get(NetdiskResourceLimitVOMeta.FUZZY_FIELD));
				this.setPageSize( (Integer)map.get(NetdiskResourceLimitVOMeta.PAGE_SIZE));
				this.setCapacitySizeValue( (String)map.get(NetdiskResourceLimitVOMeta.CAPACITY_SIZE_VALUE));
				this.setPageIndex( (Integer)map.get(NetdiskResourceLimitVOMeta.PAGE_INDEX));
				this.setSortType( (String)map.get(NetdiskResourceLimitVOMeta.SORT_TYPE));
				this.setSortField( (String)map.get(NetdiskResourceLimitVOMeta.SORT_FIELD));
				this.setDataOrigin( (String)map.get(NetdiskResourceLimitVOMeta.DATA_ORIGIN));
				this.setQueryLogic( (String)map.get(NetdiskResourceLimitVOMeta.QUERY_LOGIC));
				this.setUser( (Employee)map.get(NetdiskResourceLimitVOMeta.USER));
				this.setSearchValue( (String)map.get(NetdiskResourceLimitVOMeta.SEARCH_VALUE));
				return true;
			} catch (Exception e) {
				return false;
			}
		}
	}

	/**
	 * 从 Map 读取
	 * @param r 记录数据
	 * @param cast 是否用 DataParser 进行类型转换
	 * @return  是否读取成功
	*/
	public boolean read(ExprRcd r,boolean cast) {
		if(r==null) return false;
		if(cast) {
			this.setPlayVideoValid(DataParser.parse(String.class, r.getValue(NetdiskResourceLimitVOMeta.PLAY_VIDEO_VALID)));
			this.setPlayMusicValid(DataParser.parse(String.class, r.getValue(NetdiskResourceLimitVOMeta.PLAY_MUSIC_VALID)));
			this.setViewPhotoValid(DataParser.parse(String.class, r.getValue(NetdiskResourceLimitVOMeta.VIEW_PHOTO_VALID)));
			this.setUploadFileType(DataParser.parse(String.class, r.getValue(NetdiskResourceLimitVOMeta.UPLOAD_FILE_TYPE)));
			this.setUpdateTime(DataParser.parse(Date.class, r.getValue(NetdiskResourceLimitVOMeta.UPDATE_TIME)));
			this.setUserId(DataParser.parse(String.class, r.getValue(NetdiskResourceLimitVOMeta.USER_ID)));
			this.setCapacitySizeM(DataParser.parse(Long.class, r.getValue(NetdiskResourceLimitVOMeta.CAPACITY_SIZE_M)));
			this.setVersion(DataParser.parse(Integer.class, r.getValue(NetdiskResourceLimitVOMeta.VERSION)));
			this.setCreateBy(DataParser.parse(String.class, r.getValue(NetdiskResourceLimitVOMeta.CREATE_BY)));
			this.setUploadMaxSizeM(DataParser.parse(Long.class, r.getValue(NetdiskResourceLimitVOMeta.UPLOAD_MAX_SIZE_M)));
			this.setDeleted(DataParser.parse(Integer.class, r.getValue(NetdiskResourceLimitVOMeta.DELETED)));
			this.setCreateTime(DataParser.parse(Date.class, r.getValue(NetdiskResourceLimitVOMeta.CREATE_TIME)));
			this.setUpdateBy(DataParser.parse(String.class, r.getValue(NetdiskResourceLimitVOMeta.UPDATE_BY)));
			this.setDeleteTime(DataParser.parse(Date.class, r.getValue(NetdiskResourceLimitVOMeta.DELETE_TIME)));
			this.setCurrentSizeB(DataParser.parse(Long.class, r.getValue(NetdiskResourceLimitVOMeta.CURRENT_SIZE_B)));
			this.setTenantId(DataParser.parse(String.class, r.getValue(NetdiskResourceLimitVOMeta.TENANT_ID)));
			this.setDeleteBy(DataParser.parse(String.class, r.getValue(NetdiskResourceLimitVOMeta.DELETE_BY)));
			this.setId(DataParser.parse(String.class, r.getValue(NetdiskResourceLimitVOMeta.ID)));
			this.setViewDocValid(DataParser.parse(String.class, r.getValue(NetdiskResourceLimitVOMeta.VIEW_DOC_VALID)));
			return true;
		} else {
			try {
				this.setPlayVideoValid( (String)r.getValue(NetdiskResourceLimitVOMeta.PLAY_VIDEO_VALID));
				this.setPlayMusicValid( (String)r.getValue(NetdiskResourceLimitVOMeta.PLAY_MUSIC_VALID));
				this.setViewPhotoValid( (String)r.getValue(NetdiskResourceLimitVOMeta.VIEW_PHOTO_VALID));
				this.setUploadFileType( (String)r.getValue(NetdiskResourceLimitVOMeta.UPLOAD_FILE_TYPE));
				this.setUpdateTime( (Date)r.getValue(NetdiskResourceLimitVOMeta.UPDATE_TIME));
				this.setUserId( (String)r.getValue(NetdiskResourceLimitVOMeta.USER_ID));
				this.setCapacitySizeM( (Long)r.getValue(NetdiskResourceLimitVOMeta.CAPACITY_SIZE_M));
				this.setVersion( (Integer)r.getValue(NetdiskResourceLimitVOMeta.VERSION));
				this.setCreateBy( (String)r.getValue(NetdiskResourceLimitVOMeta.CREATE_BY));
				this.setUploadMaxSizeM( (Long)r.getValue(NetdiskResourceLimitVOMeta.UPLOAD_MAX_SIZE_M));
				this.setDeleted( (Integer)r.getValue(NetdiskResourceLimitVOMeta.DELETED));
				this.setCreateTime( (Date)r.getValue(NetdiskResourceLimitVOMeta.CREATE_TIME));
				this.setUpdateBy( (String)r.getValue(NetdiskResourceLimitVOMeta.UPDATE_BY));
				this.setDeleteTime( (Date)r.getValue(NetdiskResourceLimitVOMeta.DELETE_TIME));
				this.setCurrentSizeB( (Long)r.getValue(NetdiskResourceLimitVOMeta.CURRENT_SIZE_B));
				this.setTenantId( (String)r.getValue(NetdiskResourceLimitVOMeta.TENANT_ID));
				this.setDeleteBy( (String)r.getValue(NetdiskResourceLimitVOMeta.DELETE_BY));
				this.setId( (String)r.getValue(NetdiskResourceLimitVOMeta.ID));
				this.setViewDocValid( (String)r.getValue(NetdiskResourceLimitVOMeta.VIEW_DOC_VALID));
				return true;
			} catch (Exception e) {
				return false;
			}
		}
	}
}