package com.dt.platform.generator.config;

public class Config {
    public static int  inputLabelWidth=100;
    public static int  searchLabelWidth=100;
    public static int  searchInputWidth=180;
    public static int textAreaHeight=120;
    public static int textAreaHeight_Z=50;
    public static int textAreaHeight_60=60;
    public static int textAreaHeight_30=30;
    public static String baseFormWidth="80%";
    public static String baseFormWidth_75="75%";
    public static String baseFormWidth_50="50%";
    public static String baseFormWidth_95="95%";


}
