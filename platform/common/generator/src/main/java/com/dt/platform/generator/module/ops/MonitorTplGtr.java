package com.dt.platform.generator.module.ops;

import com.dt.platform.constants.db.EAMTables;
import com.dt.platform.constants.enums.ops.MonitorEnableEnum;
import com.dt.platform.domain.ops.*;
import com.dt.platform.domain.ops.meta.DbInfoMeta;
import com.dt.platform.domain.ops.meta.MonitorNodeTypeMeta;
import com.dt.platform.domain.ops.meta.MonitorTplMeta;
import com.dt.platform.domain.ops.meta.MonitorTplTypeMeta;
import com.dt.platform.generator.config.Config;
import com.dt.platform.ops.page.MonitorTplPageController;
import com.dt.platform.proxy.ops.MonitorNodeTypeServiceProxy;
import com.dt.platform.proxy.ops.MonitorTplServiceProxy;
import com.dt.platform.proxy.ops.MonitorTplTypeServiceProxy;
import com.github.foxnic.generator.config.WriteMode;
import com.dt.platform.constants.db.OpsTables;
public class MonitorTplGtr extends BaseCodeGenerator{


    public MonitorTplGtr() {
        super(OpsTables.OPS_MONITOR_TPL.$TABLE,MONITOR_MENU_ID);
    }

    public void generateCode() throws Exception {
        System.out.println(this.getClass().getName());

        //node type
        cfg.getPoClassFile().addSimpleProperty(MonitorTplType.class,"tplType","节点模版类型","节点模版类型");

        cfg.getPoClassFile().addListProperty(MonitorTplIndicator.class,"tplIndicatorList","指标列表","指标列表");
        cfg.getPoClassFile().addListProperty(MonitorTplGraph.class,"graphList","图形","图形");
        cfg.getPoClassFile().addListProperty(MonitorTplTrigger.class,"triggerList","触发器","触发器");

        cfg.getPoClassFile().addSimpleProperty(String.class,"indicatorCount","indicatorCount","indicatorCount");
        cfg.getPoClassFile().addSimpleProperty(String.class,"graphCount","graphCount","graphCount");
        cfg.getPoClassFile().addSimpleProperty(String.class,"triggerCount","triggerCount","triggerCount");

        cfg.view().field(MonitorTplMeta.INDICATOR_COUNT).basic().label("指标数量").table().disable(false);
        cfg.view().field(MonitorTplMeta.GRAPH_COUNT).basic().label("图形数量").table().disable(false);
        cfg.view().field(MonitorTplMeta.TRIGGER_COUNT).basic().label("触发器数量").table().disable(false);

        cfg.view().search().inputLayout(
                new Object[]{
                        OpsTables.OPS_MONITOR_TPL.TYPE,
                        OpsTables.OPS_MONITOR_TPL.STATUS,
                        OpsTables.OPS_MONITOR_TPL.NAME,
                        OpsTables.OPS_MONITOR_TPL.CODE,
                }
        );

        cfg.view().search().rowsDisplay(1);

        cfg.view().search().labelWidth(1,Config.searchLabelWidth);
        cfg.view().search().labelWidth(2,Config.searchLabelWidth);
        cfg.view().search().inputWidth(Config.searchInputWidth);

        cfg.view().field(OpsTables.OPS_MONITOR_TPL.NAME).search().fuzzySearch();
        cfg.view().field(OpsTables.OPS_MONITOR_TPL.CODE).search().fuzzySearch();

        cfg.view().field(OpsTables.OPS_MONITOR_TPL.ID).basic().hidden(true);
        cfg.view().field(OpsTables.OPS_MONITOR_TPL.ID).table().disable(true);
        cfg.view().field(OpsTables.OPS_MONITOR_TPL.CREATE_TIME).table().disable(true);
        cfg.view().field(OpsTables.OPS_MONITOR_TPL.NOTES).table().disable(true);
        cfg.view().field(OpsTables.OPS_MONITOR_TPL.CODE).table().disable(true);
        cfg.view().field(OpsTables.OPS_MONITOR_TPL.TYPE).table().disable(true);
        cfg.view().field(OpsTables.OPS_MONITOR_TPL.UPDATE_BY).table().disable(true);
        cfg.view().field(OpsTables.OPS_MONITOR_TPL.STATUS).form().validate().required().form()
                .label("状态").radioBox().defaultIndex(0).enumType(MonitorEnableEnum.class);


        cfg.view().field(OpsTables.OPS_MONITOR_TPL.TYPE)
                .basic().label("归类")
                .form().validate().required().form().selectBox().queryApi(MonitorTplTypeServiceProxy.QUERY_PAGED_LIST)
                .paging(true).filter(true).toolbar(false)
                .valueField(MonitorTplTypeMeta.CODE).
                textField(MonitorTplTypeMeta.NAME).
                fillWith(MonitorTplMeta.TPL_TYPE).muliti(false);


        cfg.view().field(OpsTables.OPS_MONITOR_TPL.CODE).form().validate().required();
        cfg.view().field(OpsTables.OPS_MONITOR_TPL.NAME).form().validate().required();
        cfg.view().field(OpsTables.OPS_MONITOR_TPL.NOTES).form().textArea().height(450);
        cfg.view().formWindow().bottomSpace(120);

        cfg.view().formWindow().width(Config.baseFormWidth);;
        cfg.view().form().addGroup(null,
                new Object[] {
                        OpsTables.OPS_MONITOR_TPL.TYPE,
                        OpsTables.OPS_MONITOR_TPL.STATUS,
                        OpsTables.OPS_MONITOR_TPL.NAME,
                        OpsTables.OPS_MONITOR_TPL.CODE,
                        OpsTables.OPS_MONITOR_TPL.NOTES,
                }
        );
        cfg.view().list().operationColumn().addActionButton("指标","items","items-button","ops_monitor_tpl:items");
        cfg.view().list().operationColumn().addActionButton("触发器","trigger","trigger-button","ops_monitor_tpl:trigger");
        cfg.view().list().operationColumn().addActionButton("图形","graph","graph-button","ops_monitor_tpl:graph");

        //文件生成覆盖模式
        cfg.overrides()
                .setServiceIntfAnfImpl(WriteMode.IGNORE) //服务与接口
                .setControllerAndAgent(WriteMode.IGNORE) //Rest
                .setPageController(WriteMode.IGNORE) //页面控制器
                .setBpmEventAdaptor(WriteMode.IGNORE) //页面控制器
                .setFormPage(WriteMode.COVER_EXISTS_FILE) //表单HTML页
                .setListPage(WriteMode.COVER_EXISTS_FILE)//列表HTML页
                .setExtendJsFile(WriteMode.IGNORE); //列表HTML页
        //生成代码
        cfg.buildAll();
    }

    public static void main(String[] args) throws Exception {
        MonitorTplGtr g=new MonitorTplGtr();
        //生成代码
        g.generateCode();

        //移除之前生成的菜单，视情况执行
       // g.generateMenu(MonitorTplServiceProxy.class, MonitorTplPageController.class);
//        delete from sys_menu_resource where resource_id in (select id from sys_resourze where batch_id='541696318553194496');
//        delete from sys_role_menu where menu_id in (select id from sys_menu where batch_id='541696318553194496');
//        delete from sys_menu where batch_id='541696318553194496';
//        delete from sys_resourze where batch_id='541696318553194496'

    }
}
