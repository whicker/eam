package com.dt.platform.common.controller;

import java.util.*;
import org.github.foxnic.web.framework.web.SuperController;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import com.github.foxnic.commons.collection.CollectorUtil;
import com.github.foxnic.dao.entity.ReferCause;
import com.github.foxnic.api.swagger.InDoc;
import org.github.foxnic.web.framework.sentinel.SentinelExceptionUtil;
import com.github.foxnic.api.swagger.ApiParamSupport;
import com.alibaba.csp.sentinel.annotation.SentinelResource;


import com.dt.platform.proxy.common.ReportAclServiceProxy;
import com.dt.platform.domain.common.meta.ReportAclVOMeta;
import com.dt.platform.domain.common.ReportAcl;
import com.dt.platform.domain.common.ReportAclVO;
import com.github.foxnic.api.transter.Result;
import com.github.foxnic.dao.data.SaveMode;
import com.github.foxnic.dao.excel.ExcelWriter;
import com.github.foxnic.springboot.web.DownloadUtil;
import com.github.foxnic.dao.data.PagedList;
import java.util.Date;
import java.sql.Timestamp;
import com.github.foxnic.api.error.ErrorDesc;
import com.github.foxnic.commons.io.StreamUtil;
import java.util.Map;
import com.github.foxnic.dao.excel.ValidateResult;
import java.io.InputStream;
import com.dt.platform.domain.common.meta.ReportAclMeta;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiImplicitParam;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.dt.platform.common.service.IReportAclService;
import com.github.foxnic.api.validate.annotations.NotNull;

/**
 * <p>
 * 报表ACL接口控制器
 * </p>
 * @author 金杰 , maillank@qq.com
 * @since 2023-12-28 19:53:14
*/

@InDoc
@Api(tags = "报表ACL")
@RestController("SysReportAclController")
public class ReportAclController extends SuperController {

	@Autowired
	private IReportAclService reportAclService;

	/**
	 * 添加报表ACL
	*/
	@ApiOperation(value = "添加报表ACL")
	@ApiImplicitParams({
		@ApiImplicitParam(name = ReportAclVOMeta.ID , value = "主键" , required = true , dataTypeClass=String.class),
		@ApiImplicitParam(name = ReportAclVOMeta.REPORT_ID , value = "报表" , required = false , dataTypeClass=String.class),
		@ApiImplicitParam(name = ReportAclVOMeta.IP , value = "IP" , required = false , dataTypeClass=String.class),
		@ApiImplicitParam(name = ReportAclVOMeta.NOTES , value = "备注" , required = false , dataTypeClass=String.class),
		@ApiImplicitParam(name = ReportAclVOMeta.UPDATE_BY , value = "修改人ID" , required = false , dataTypeClass=String.class),
	})
	@ApiParamSupport(ignoreDBTreatyProperties = true, ignoreDefaultVoProperties = true , ignorePrimaryKey = true)
	@ApiOperationSupport(order=1 , author="金杰 , maillank@qq.com")
	@SentinelResource(value = ReportAclServiceProxy.INSERT , blockHandlerClass = { SentinelExceptionUtil.class } , blockHandler = SentinelExceptionUtil.HANDLER )
	@PostMapping(ReportAclServiceProxy.INSERT)
	public Result insert(ReportAclVO reportAclVO) {
		
		Result result=reportAclService.insert(reportAclVO,false);
		return result;
	}



	/**
	 * 删除报表ACL
	*/
	@ApiOperation(value = "删除报表ACL")
	@ApiImplicitParams({
		@ApiImplicitParam(name = ReportAclVOMeta.ID , value = "主键" , required = true , dataTypeClass=String.class)
	})
	@ApiOperationSupport(order=2 , author="金杰 , maillank@qq.com")
	@SentinelResource(value = ReportAclServiceProxy.DELETE , blockHandlerClass = { SentinelExceptionUtil.class } , blockHandler = SentinelExceptionUtil.HANDLER )
	@PostMapping(ReportAclServiceProxy.DELETE)
	public Result deleteById(String id) {
		
		this.validator().asserts(id).require("缺少id值");
		if(this.validator().failure()) {
			return this.validator().getFirstResult();
		}
		// 引用校验
		ReferCause cause =  reportAclService.hasRefers(id);
		// 判断是否可以删除
		this.validator().asserts(cause.hasRefer()).requireEqual("不允许删除当前记录："+cause.message(),false);
		if(this.validator().failure()) {
			return this.validator().getFirstResult().messageLevel4Confirm();
		}
		Result result=reportAclService.deleteByIdLogical(id);
		return result;
	}


	/**
	 * 批量删除报表ACL <br>
	 * 联合主键时，请自行调整实现
	*/
	@ApiOperation(value = "批量删除报表ACL")
	@ApiImplicitParams({
		@ApiImplicitParam(name = ReportAclVOMeta.IDS , value = "主键清单" , required = true , dataTypeClass=List.class , example = "[1,3,4]")
	})
	@ApiOperationSupport(order=3 , author="金杰 , maillank@qq.com") 
	@SentinelResource(value = ReportAclServiceProxy.DELETE_BY_IDS , blockHandlerClass = { SentinelExceptionUtil.class } , blockHandler = SentinelExceptionUtil.HANDLER )
	@PostMapping(ReportAclServiceProxy.DELETE_BY_IDS)
	public Result deleteByIds(List<String> ids) {
		
		// 参数校验
		this.validator().asserts(ids).require("缺少ids参数");
		if(this.validator().failure()) {
			return this.validator().getFirstResult();
		}

		// 查询引用
		Map<String, ReferCause> causeMap = reportAclService.hasRefers(ids);
		// 收集可以删除的ID值
		List<String> canDeleteIds = new ArrayList<>();
		for (Map.Entry<String, ReferCause> e : causeMap.entrySet()) {
			if (!e.getValue().hasRefer()) {
				canDeleteIds.add(e.getKey());
			}
		}

		// 执行删除
		if (canDeleteIds.isEmpty()) {
			// 如果没有一行可以被删除
			return ErrorDesc.failure().message("无法删除您选中的数据行：").data(0)
				.addErrors(CollectorUtil.collectArray(CollectorUtil.filter(causeMap.values(),(e)->{return e.hasRefer();}),ReferCause::message,String.class))
				.messageLevel4Confirm();
		} else if (canDeleteIds.size() == ids.size()) {
			// 如果全部可以删除
			Result result=reportAclService.deleteByIdsLogical(canDeleteIds);
			return result;
		} else if (canDeleteIds.size()>0 && canDeleteIds.size() < ids.size()) {
			// 如果部分行可以删除
			Result result=reportAclService.deleteByIdsLogical(canDeleteIds);
			if (result.failure()) {
				return result;
			} else {
				return ErrorDesc.success().message("已删除 " + canDeleteIds.size() + " 行，但另有 " + (ids.size() - canDeleteIds.size()) + " 行数据无法删除").data(canDeleteIds.size())
				.addErrors(CollectorUtil.collectArray(CollectorUtil.filter(causeMap.values(),(e)->{return e.hasRefer();}),ReferCause::message,String.class))
				.messageLevel4Confirm();
			}
		} else {
			// 理论上，这个分支不存在
			return ErrorDesc.success().message("数据删除未处理");
		}
	}

	/**
	 * 更新报表ACL
	*/
	@ApiOperation(value = "更新报表ACL")
	@ApiImplicitParams({
		@ApiImplicitParam(name = ReportAclVOMeta.ID , value = "主键" , required = true , dataTypeClass=String.class),
		@ApiImplicitParam(name = ReportAclVOMeta.REPORT_ID , value = "报表" , required = false , dataTypeClass=String.class),
		@ApiImplicitParam(name = ReportAclVOMeta.IP , value = "IP" , required = false , dataTypeClass=String.class),
		@ApiImplicitParam(name = ReportAclVOMeta.NOTES , value = "备注" , required = false , dataTypeClass=String.class),
		@ApiImplicitParam(name = ReportAclVOMeta.UPDATE_BY , value = "修改人ID" , required = false , dataTypeClass=String.class),
	})
	@ApiParamSupport(ignoreDBTreatyProperties = true, ignoreDefaultVoProperties = true)
	@ApiOperationSupport( order=4 , author="金杰 , maillank@qq.com" ,  ignoreParameters = { ReportAclVOMeta.PAGE_INDEX , ReportAclVOMeta.PAGE_SIZE , ReportAclVOMeta.SEARCH_FIELD , ReportAclVOMeta.FUZZY_FIELD , ReportAclVOMeta.SEARCH_VALUE , ReportAclVOMeta.DIRTY_FIELDS , ReportAclVOMeta.SORT_FIELD , ReportAclVOMeta.SORT_TYPE , ReportAclVOMeta.DATA_ORIGIN , ReportAclVOMeta.QUERY_LOGIC , ReportAclVOMeta.REQUEST_ACTION , ReportAclVOMeta.IDS } )
	@SentinelResource(value = ReportAclServiceProxy.UPDATE , blockHandlerClass = { SentinelExceptionUtil.class } , blockHandler = SentinelExceptionUtil.HANDLER )
	@PostMapping(ReportAclServiceProxy.UPDATE)
	public Result update(ReportAclVO reportAclVO) {
		
		Result result=reportAclService.update(reportAclVO,SaveMode.DIRTY_OR_NOT_NULL_FIELDS,false);
		return result;
	}


	/**
	 * 保存报表ACL
	*/
	@ApiOperation(value = "保存报表ACL")
	@ApiImplicitParams({
		@ApiImplicitParam(name = ReportAclVOMeta.ID , value = "主键" , required = true , dataTypeClass=String.class),
		@ApiImplicitParam(name = ReportAclVOMeta.REPORT_ID , value = "报表" , required = false , dataTypeClass=String.class),
		@ApiImplicitParam(name = ReportAclVOMeta.IP , value = "IP" , required = false , dataTypeClass=String.class),
		@ApiImplicitParam(name = ReportAclVOMeta.NOTES , value = "备注" , required = false , dataTypeClass=String.class),
		@ApiImplicitParam(name = ReportAclVOMeta.UPDATE_BY , value = "修改人ID" , required = false , dataTypeClass=String.class),
	})
	@ApiParamSupport(ignoreDBTreatyProperties = true, ignoreDefaultVoProperties = true)
	@ApiOperationSupport(order=5 ,  ignoreParameters = { ReportAclVOMeta.PAGE_INDEX , ReportAclVOMeta.PAGE_SIZE , ReportAclVOMeta.SEARCH_FIELD , ReportAclVOMeta.FUZZY_FIELD , ReportAclVOMeta.SEARCH_VALUE , ReportAclVOMeta.DIRTY_FIELDS , ReportAclVOMeta.SORT_FIELD , ReportAclVOMeta.SORT_TYPE , ReportAclVOMeta.DATA_ORIGIN , ReportAclVOMeta.QUERY_LOGIC , ReportAclVOMeta.REQUEST_ACTION , ReportAclVOMeta.IDS } )
	@SentinelResource(value = ReportAclServiceProxy.SAVE , blockHandlerClass = { SentinelExceptionUtil.class } , blockHandler = SentinelExceptionUtil.HANDLER )
	@PostMapping(ReportAclServiceProxy.SAVE)
	public Result save(ReportAclVO reportAclVO) {
		
		Result result=reportAclService.save(reportAclVO,SaveMode.DIRTY_OR_NOT_NULL_FIELDS,false);
		return result;
	}


	/**
	 * 获取报表ACL
	*/
	@ApiOperation(value = "获取报表ACL")
	@ApiImplicitParams({
		@ApiImplicitParam(name = ReportAclVOMeta.ID , value = "主键" , required = true , dataTypeClass=String.class , example = "1"),
	})
	@ApiOperationSupport(order=6 , author="金杰 , maillank@qq.com")
	@SentinelResource(value = ReportAclServiceProxy.GET_BY_ID , blockHandlerClass = { SentinelExceptionUtil.class } , blockHandler = SentinelExceptionUtil.HANDLER )
	@PostMapping(ReportAclServiceProxy.GET_BY_ID)
	public Result<ReportAcl> getById(String id) {
		
		Result<ReportAcl> result=new Result<>();
		ReportAcl reportAcl=reportAclService.getById(id);
		result.success(true).data(reportAcl);
		return result;
	}


	/**
	 * 批量获取报表ACL <br>
	 * 联合主键时，请自行调整实现
	*/
		@ApiOperation(value = "批量获取报表ACL")
		@ApiImplicitParams({
				@ApiImplicitParam(name = ReportAclVOMeta.IDS , value = "主键清单" , required = true , dataTypeClass=List.class , example = "[1,3,4]")
		})
		@ApiOperationSupport(order=3 , author="金杰 , maillank@qq.com") 
		@SentinelResource(value = ReportAclServiceProxy.GET_BY_IDS , blockHandlerClass = { SentinelExceptionUtil.class } , blockHandler = SentinelExceptionUtil.HANDLER )
	@PostMapping(ReportAclServiceProxy.GET_BY_IDS)
	public Result<List<ReportAcl>> getByIds(List<String> ids) {
		
		Result<List<ReportAcl>> result=new Result<>();
		List<ReportAcl> list=reportAclService.queryListByIds(ids);
		result.success(true).data(list);
		return result;
	}


	/**
	 * 查询报表ACL
	*/
	@ApiOperation(value = "查询报表ACL")
	@ApiImplicitParams({
		@ApiImplicitParam(name = ReportAclVOMeta.ID , value = "主键" , required = true , dataTypeClass=String.class),
		@ApiImplicitParam(name = ReportAclVOMeta.REPORT_ID , value = "报表" , required = false , dataTypeClass=String.class),
		@ApiImplicitParam(name = ReportAclVOMeta.IP , value = "IP" , required = false , dataTypeClass=String.class),
		@ApiImplicitParam(name = ReportAclVOMeta.NOTES , value = "备注" , required = false , dataTypeClass=String.class),
		@ApiImplicitParam(name = ReportAclVOMeta.UPDATE_BY , value = "修改人ID" , required = false , dataTypeClass=String.class),
	})
	@ApiOperationSupport(order=5 , author="金杰 , maillank@qq.com" ,  ignoreParameters = { ReportAclVOMeta.PAGE_INDEX , ReportAclVOMeta.PAGE_SIZE } )
	@SentinelResource(value = ReportAclServiceProxy.QUERY_LIST , blockHandlerClass = { SentinelExceptionUtil.class } , blockHandler = SentinelExceptionUtil.HANDLER )
	@PostMapping(ReportAclServiceProxy.QUERY_LIST)
	public Result<List<ReportAcl>> queryList(ReportAclVO sample) {
		
		Result<List<ReportAcl>> result=new Result<>();
		List<ReportAcl> list=reportAclService.queryList(sample);
		result.success(true).data(list);
		return result;
	}


	/**
	 * 分页查询报表ACL
	*/
	@ApiOperation(value = "分页查询报表ACL")
	@ApiImplicitParams({
		@ApiImplicitParam(name = ReportAclVOMeta.ID , value = "主键" , required = true , dataTypeClass=String.class),
		@ApiImplicitParam(name = ReportAclVOMeta.REPORT_ID , value = "报表" , required = false , dataTypeClass=String.class),
		@ApiImplicitParam(name = ReportAclVOMeta.IP , value = "IP" , required = false , dataTypeClass=String.class),
		@ApiImplicitParam(name = ReportAclVOMeta.NOTES , value = "备注" , required = false , dataTypeClass=String.class),
		@ApiImplicitParam(name = ReportAclVOMeta.UPDATE_BY , value = "修改人ID" , required = false , dataTypeClass=String.class),
	})
	@ApiOperationSupport(order=8 , author="金杰 , maillank@qq.com")
	@SentinelResource(value = ReportAclServiceProxy.QUERY_PAGED_LIST , blockHandlerClass = { SentinelExceptionUtil.class } , blockHandler = SentinelExceptionUtil.HANDLER )
	@PostMapping(ReportAclServiceProxy.QUERY_PAGED_LIST)
	public Result<PagedList<ReportAcl>> queryPagedList(ReportAclVO sample) {
		
		Result<PagedList<ReportAcl>> result=new Result<>();
		PagedList<ReportAcl> list=reportAclService.queryPagedList(sample,sample.getPageSize(),sample.getPageIndex());
		result.success(true).data(list);
		return result;
	}





}