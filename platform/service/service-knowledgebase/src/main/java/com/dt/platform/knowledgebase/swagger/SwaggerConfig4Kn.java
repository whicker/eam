package com.dt.platform.knowledgebase.swagger;

import com.dt.platform.framework.PlatformMeta;
import com.github.xiaoymin.knife4j.spring.extension.OpenApiExtensionResolver;
import org.github.foxnic.web.framework.knife4j.BasicSwaggerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spring.web.plugins.Docket;


/**
 * Swagger API 相关配置
 */
@Configuration
public class SwaggerConfig4Kn extends BasicSwaggerConfig {

    @Autowired
    public SwaggerConfig4Kn(OpenApiExtensionResolver openApiExtensionResolver) {
        super(openApiExtensionResolver, PlatformMeta.KNOWLEDGEBASE_PACKAGE);
    }


    @Bean("KnApiDocket")
    public Docket createRestApi() {
        return super.createRestApi();
    }
    protected ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("知识库")
                .description("知识库相关的接口。")
                .version(PlatformMeta.VERSION)
                .build();
    }

}
