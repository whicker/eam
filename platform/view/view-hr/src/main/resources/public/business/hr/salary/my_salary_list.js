/**
 * 人员薪酬 列表页 JS 脚本
 * @author 金杰 , maillank@qq.com
 * @since 2023-06-04 13:57:33
 */


function ListPage() {

    var settings,admin,form,table,layer,util,fox,upload,xmSelect;

    //模块基础路径
    const moduleURL="/service-hr/hr-salary";
    const queryURL=moduleURL+'/my-query-paged-list';
    const deleteURL=moduleURL+'/delete';
    const batchDeleteURL=moduleURL+'/delete-by-ids';
    const getByIdURL=moduleURL+'/get-by-id';
    //
    var dataTable=null;
    var sort=null;

    /**
     * 入口函数，初始化
     */
    this.init=function(layui) {

        admin = layui.admin,settings = layui.settings,form = layui.form,upload = layui.upload,laydate= layui.laydate;
        table = layui.table,layer = layui.layer,util = layui.util,fox = layui.foxnic,xmSelect = layui.xmSelect,dropdown=layui.dropdown;

        if(window.pageExt.list.beforeInit) {
            window.pageExt.list.beforeInit();
        }
        //渲染表格
        renderTable();
        //初始化搜索输入框组件
        initSearchFields();
        //绑定搜索框事件
        bindSearchEvent();
        //绑定按钮事件
        bindButtonEvent();
        //绑定行操作按钮事件
        bindRowOperationEvent();
    }


    /**
     * 渲染表格
     */
    function renderTable() {
        $(window).resize(function() {
            fox.adjustSearchElement();
        });
        fox.adjustSearchElement();
        //
        var marginTop=$(".search-bar").height()+$(".search-bar").css("padding-top")+$(".search-bar").css("padding-bottom")
        $("#table-area").css("margin-top",marginTop+"px");
        //
        function renderTableInternal() {

            var ps={searchField: "$composite"};
            var contitions={};

            if(window.pageExt.list.beforeQuery){
                window.pageExt.list.beforeQuery(contitions,ps,"tableInit");
            }
            ps.searchValue=JSON.stringify(contitions);

            var templet=window.pageExt.list.templet;
            if(templet==null) {
                templet=function(field,value,row) {
                    if(value==null) return "";
                    return value;
                }
            }
            var h=$(".search-bar").height();
            var tableConfig={
                elem: '#data-table',
                toolbar: '#toolbarTemplate',
                defaultToolbar: ['filter', 'print',{title: fox.translate('刷新数据','','cmp:table'),layEvent: 'refresh-data',icon: 'layui-icon-refresh-3'}],
                url: queryURL,
                height: 'full-'+(h+28),
                limit: 50,
                where: ps,
                cols: [[
                    { fixed: 'left',type: 'numbers' }
                  //  { fixed: 'left',type:'checkbox'}
                    ,{ field: 'id', align:"left",fixed:false,  hide:true, sort: true  , title: fox.translate('主键') , templet: function (d) { return templet('id',d.id,d);}  }
                    ,{ field: 'personId', align:"left",fixed:false,  hide:false, sort: true  , title: fox.translate('人员') , templet: function (d) { return templet('personId',fox.getProperty(d,["person","name"],0,'','personId'),d);} }
                    ,{ field: 'baseSalary', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('基本工资') , templet: function (d) { return templet('baseSalary',d.baseSalary,d);}  }
                    ,{ field: 'postSalary', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('岗位工资') , templet: function (d) { return templet('postSalary',d.postSalary,d);}  }
                    ,{ field: 'workingYearsSalary', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('工龄工资') , templet: function (d) { return templet('workingYearsSalary',d.workingYearsSalary,d);}  }
                    ,{ field: 'fixedSalary', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('固定补贴') , templet: function (d) { return templet('fixedSalary',d.fixedSalary,d);}  }
                    ,{ field: 'achievementSalary', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('绩效补贴') , templet: function (d) { return templet('achievementSalary',d.achievementSalary,d);}  }
                    ,{ field: 'overtimeSalary', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('加班补贴') , templet: function (d) { return templet('overtimeSalary',d.overtimeSalary,d);}  }
                    ,{ field: 'otherSalary', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('其他补贴') , templet: function (d) { return templet('otherSalary',d.otherSalary,d);}  }
                    ,{ field: 'communicationSalary', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('通讯补贴') , templet: function (d) { return templet('communicationSalary',d.communicationSalary,d);}  }
                    ,{ field: 'trafficSalary', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('交通补贴') , templet: function (d) { return templet('trafficSalary',d.trafficSalary,d);}  }
                    ,{ field: 'housingSalary', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('住房补贴') , templet: function (d) { return templet('housingSalary',d.housingSalary,d);}  }
                    ,{ field: 'commissionSalary', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('提成工资') , templet: function (d) { return templet('commissionSalary',d.commissionSalary,d);}  }
                    ,{ field: 'highTemperatureSalary', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('高温补贴') , templet: function (d) { return templet('highTemperatureSalary',d.highTemperatureSalary,d);}  }
                    ,{ field: 'welfareZfgjjBase', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('住房公积金基数') , templet: function (d) { return templet('welfareZfgjjBase',d.welfareZfgjjBase,d);}  }
                    ,{ field: 'welfareZfgjjPerson', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('住房公积金个人') , templet: function (d) { return templet('welfareZfgjjPerson',d.welfareZfgjjPerson,d);}  }
                    ,{ field: 'welfareZfgjjCompany', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('住房公积金公司') , templet: function (d) { return templet('welfareZfgjjCompany',d.welfareZfgjjCompany,d);}  }
                    ,{ field: 'welfaerYlbxBase', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('养老保险基数') , templet: function (d) { return templet('welfaerYlbxBase',d.welfaerYlbxBase,d);}  }
                    ,{ field: 'welfaerYlbxPerson', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('养老保险个人') , templet: function (d) { return templet('welfaerYlbxPerson',d.welfaerYlbxPerson,d);}  }
                    ,{ field: 'welfaerYlbxCompany', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('养老保险公司') , templet: function (d) { return templet('welfaerYlbxCompany',d.welfaerYlbxCompany,d);}  }
                    ,{ field: 'welfaerGsbxBase', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('工伤保险基数') , templet: function (d) { return templet('welfaerGsbxBase',d.welfaerGsbxBase,d);}  }
                    ,{ field: 'welfaerGsbxPerson', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('工伤保险个人') , templet: function (d) { return templet('welfaerGsbxPerson',d.welfaerGsbxPerson,d);}  }
                    ,{ field: 'welfaerGsbxCompany', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('工伤保险公司') , templet: function (d) { return templet('welfaerGsbxCompany',d.welfaerGsbxCompany,d);}  }
                    ,{ field: 'welfaerYrbxBase', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('医疗保险基数') , templet: function (d) { return templet('welfaerYrbxBase',d.welfaerYrbxBase,d);}  }
                    ,{ field: 'welfaerYrbxPerson', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('医疗保险个人') , templet: function (d) { return templet('welfaerYrbxPerson',d.welfaerYrbxPerson,d);}  }
                    ,{ field: 'welfaerYrbxCompany', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('医疗保险公司') , templet: function (d) { return templet('welfaerYrbxCompany',d.welfaerYrbxCompany,d);}  }
                    ,{ field: 'welfaerSybxBase', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('生育保险基数') , templet: function (d) { return templet('welfaerSybxBase',d.welfaerSybxBase,d);}  }
                    ,{ field: 'welfaerSybxPerson', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('生育保险个人') , templet: function (d) { return templet('welfaerSybxPerson',d.welfaerSybxPerson,d);}  }
                    ,{ field: 'welfaerSybxCompany', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('生育保险公司') , templet: function (d) { return templet('welfaerSybxCompany',d.welfaerSybxCompany,d);}  }
                    ,{ field: 'welfaerSyebxBase', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('失业保险基数') , templet: function (d) { return templet('welfaerSyebxBase',d.welfaerSyebxBase,d);}  }
                    ,{ field: 'welfaerSyebxPerson', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('失业保险个人') , templet: function (d) { return templet('welfaerSyebxPerson',d.welfaerSyebxPerson,d);}  }
                    ,{ field: 'welfaerSyebxCompany', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('失业保险公司') , templet: function (d) { return templet('welfaerSyebxCompany',d.welfaerSyebxCompany,d);}  }
                    ,{ field: 'deductPersonalTaxRed', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('个税抵扣') , templet: function (d) { return templet('deductPersonalTaxRed',d.deductPersonalTaxRed,d);}  }
                    ,{ field: 'deductKq', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('扣除考勤') , templet: function (d) { return templet('deductKq',d.deductKq,d);}  }
                    ,{ field: 'deductGh', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('扣除工会') , templet: function (d) { return templet('deductGh',d.deductGh,d);}  }
                    ,{ field: 'duductOther', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('扣除其他') , templet: function (d) { return templet('duductOther',d.duductOther,d);}  }
                    ,{ field: 'personalTaxZnjy', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('子女教育') , templet: function (d) { return templet('personalTaxZnjy',d.personalTaxZnjy,d);}  }
                    ,{ field: 'personalTaxJxjy', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('继续教育') , templet: function (d) { return templet('personalTaxJxjy',d.personalTaxJxjy,d);}  }
                    ,{ field: 'personalTaxDbyl', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('大病医疗') , templet: function (d) { return templet('personalTaxDbyl',d.personalTaxDbyl,d);}  }
                    ,{ field: 'personalTaxZfdk', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('住房贷款') , templet: function (d) { return templet('personalTaxZfdk',d.personalTaxZfdk,d);}  }
                    ,{ field: 'personalTaxZfzj', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('住房租金') , templet: function (d) { return templet('personalTaxZfzj',d.personalTaxZfzj,d);}  }
                    ,{ field: 'personalTaxSylr', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('赡养老人') , templet: function (d) { return templet('personalTaxSylr',d.personalTaxSylr,d);}  }
                    ,{ field: 'personalTaxErzh', align:"right",fixed:false,  hide:false, sort: true  , title: fox.translate('幼儿照护') , templet: function (d) { return templet('personalTaxErzh',d.personalTaxErzh,d);}  }
                    ,{ field: 'createTime', align:"right", fixed:false, hide:false, sort: true   ,title: fox.translate('创建时间') ,templet: function (d) { return templet('createTime',fox.dateFormat(d.createTime,"yyyy-MM-dd HH:mm:ss"),d); }  }
                    ,{ field: fox.translate('空白列','','cmp:table'), align:"center", hide:false, sort: false, title: "",minWidth:8,width:8,unresize:true}
                    ,{ field: 'row-ops', fixed: 'right', align: 'center', toolbar: '#tableOperationTemplate', title: fox.translate('操作','','cmp:table'), width: 160 }
                ]],
                done: function (data) {
                    lockSwitchInputs();
                    window.pageExt.list.afterQuery && window.pageExt.list.afterQuery(data);
                },
                footer : {
                    exportExcel : false ,
                    importExcel : false
                }
            };
            window.pageExt.list.beforeTableRender && window.pageExt.list.beforeTableRender(tableConfig);
            dataTable=fox.renderTable(tableConfig);
            //绑定排序事件
            table.on('sort(data-table)', function(obj){
                refreshTableData(obj.sortField,obj.type);
            });
            window.pageExt.list.afterTableRender && window.pageExt.list.afterTableRender();
        }
        setTimeout(renderTableInternal,1);
    };

    /**
     * 刷新单号数据
     * */
    function refreshRowData(data,remote) {
        var context=dataTable.getDataRowContext( { id : data.id } );
        if(context==null) return;
        if(remote) {
            admin.post(getByIdURL, { id : data.id }, function (r) {
                if (r.success) {
                    data = r.data;
                    context.update(data);
                    fox.renderFormInputs(form);
                    lockSwitchInputs();
                    window.pageExt.list.afterRefreshRowData && window.pageExt.list.afterRefreshRowData(data,remote,context);
                } else {
                    fox.showMessage(data);
                }
            });
        } else {
            context.update(data);
            fox.renderFormInputs(form);
            lockSwitchInputs();
            window.pageExt.list.afterRefreshRowData && window.pageExt.list.afterRefreshRowData(data,remote,context);
        }
    }



    function lockSwitchInputs() {
    }

    function lockSwitchInput(field) {
        var inputs=$("[lay-id=data-table]").find("td[data-field='"+field+"']").find("input");
        var switchs=$("[lay-id=data-table]").find("td[data-field='"+field+"']").find(".layui-form-switch");
        inputs.attr("readonly", "yes");
        inputs.attr("disabled", "yes");
        switchs.addClass("layui-disabled");
        switchs.addClass("layui-checkbox-disabled");
        switchs.addClass("layui-form-switch-disabled");
    }

    /**
     * 刷新表格数据
     */
    function refreshTableData(sortField,sortType,reset) {
        function getSelectedValue(id,prop) { var xm=xmSelect.get(id,true); return xm==null ? null : xm.getValue(prop);}
        var value = {};
        value.personId={ inputType:"button",value: $("#personId").val(),fillBy:["person","name"] ,label:$("#personId-button").text() };
        var ps={searchField:"$composite"};
        if(window.pageExt.list.beforeQuery){
            if(!window.pageExt.list.beforeQuery(value,ps,"refresh")) return;
        }
        ps.searchValue=JSON.stringify(value);
        if(sortField) {
            ps.sortField=sortField;
            ps.sortType=sortType;
            sort={ field : sortField,type : sortType} ;
        } else {
            if(sort) {
                ps.sortField=sort.field;
                ps.sortType=sort.type;
            } 		}
        if(reset) {
            table.reload('data-table', { where : ps , page:{ curr:1 } });
        } else {
            table.reload('data-table', { where : ps });
        }
    }


    /**
     * 获得已经选中行的数据,不传入 field 时，返回所有选中的记录，指定 field 时 返回指定的字段集合
     */
    function getCheckedList(field) {
        var checkStatus = table.checkStatus('data-table');
        var data = checkStatus.data;
        if(!field) return data;
        for(var i=0;i<data.length;i++) data[i]=data[i][field];
        return data;
    }

    /**
     * 重置搜索框
     */
    function resetSearchFields() {
        $('#search-field').val("");
        $('#search-input').val("");
        layui.form.render();
    }

    function initSearchFields() {

        fox.switchSearchRow(1);

        fox.renderSearchInputs();
        window.pageExt.list.afterSearchInputReady && window.pageExt.list.afterSearchInputReady();
    }

    /**
     * 绑定搜索框事件
     */
    function bindSearchEvent() {
        //回车键查询
        $(".search-input").keydown(function(event) {
            if(event.keyCode !=13) return;
            refreshTableData(null,null,true);
        });

        // 搜索按钮点击事件
        $('#search-button').click(function () {
            refreshTableData(null,null,true);
        });

        // 搜索按钮点击事件
        $('#search-button-advance').click(function () {
            fox.switchSearchRow(1,function (ex){
                if(ex=="1") {
                    $('#search-button-advance span').text("关闭");
                } else {
                    $('#search-button-advance span').text("更多");
                }
            });
        });

        // 请选择人员对话框
        $("#personId-button").click(function(){
            var personIdDialogOptions={
                field:"personId",
                inputEl:$("#personId"),
                buttonEl:$(this),
                single:false,
                //限制浏览的范围，指定根节点 id 或 code ，优先匹配ID
                root: "",
                targetType:"emp",
                prepose:function(param){ return window.pageExt.list.beforeDialog && window.pageExt.list.beforeDialog(param);},
                callback:function(param,result){ window.pageExt.list.afterDialog && window.pageExt.list.afterDialog(param,result);}
            };
            fox.chooseEmployee(personIdDialogOptions);
        });
    }

    /**
     * 绑定按钮事件
     */
    function bindButtonEvent() {

        //头工具栏事件
        table.on('toolbar(data-table)', function(obj){
            var checkStatus = table.checkStatus(obj.config.id);
            var selected=getCheckedList("id");
            if(window.pageExt.list.beforeToolBarButtonEvent) {
                var doNext=window.pageExt.list.beforeToolBarButtonEvent(selected,obj);
                if(!doNext) return;
            }
            switch(obj.event){
                case 'salary-detail':
                    var action="view"
                    var queryString="";
                    admin.putTempData('hr-salary-form-data', {});
                    var title = fox.translate('薪酬明细');
                    admin.popupCenter({
                        title: title,
                        resize: false,
                        offset: [20,null],
                        area: ["85%","85%"],
                        type: 2,
                        id:"hr-salary-dtl-form-data-win",
                        content: '/business/hr/salary/my_salary_dtl_list.html' + (queryString?("?"+queryString):""),
                        finish: function () {
                        }
                    });
                    break;
                case 'batch-del':
                    batchDelete(selected);
                    break;
                case 'refresh-data':
                    refreshTableData();
                    break;
                case 'other':
                    break;
            };
        });


        //添加按钮点击事件
        function openCreateFrom() {
            //设置新增是初始化数据
            var data={};
            admin.putTempData('hr-salary-form-data-form-action', "create",true);
            showEditForm(data);
        };

        //批量删除按钮点击事件
        function batchDelete(selected) {

            if(window.pageExt.list.beforeBatchDelete) {
                var doNext=window.pageExt.list.beforeBatchDelete(selected);
                if(!doNext) return;
            }

            var ids=getCheckedList("id");
            if(ids.length==0) {
                top.layer.msg(fox.translate('请选择需要删除的'+'人员薪酬'+"!"));
                return;
            }
            //调用批量删除接口
            top.layer.confirm(fox.translate('确定删除已选中的'+'人员薪酬'+'吗？'), function (i) {
                top.layer.close(i);
                admin.post(batchDeleteURL, { ids: ids }, function (data) {
                    if (data.success) {
                        if(window.pageExt.list.afterBatchDelete) {
                            var doNext=window.pageExt.list.afterBatchDelete(data);
                            if(!doNext) return;
                        }
                        fox.showMessage(data);
                        refreshTableData();
                    } else {
                        if(data.data>0) {
                            refreshTableData();
                        }
                        fox.showMessage(data);
                    }
                },{delayLoading:200,elms:[$("#delete-button")]});
            });
        }
    }

    /**
     * 绑定行操作按钮事件
     */
    function bindRowOperationEvent() {
        // 工具条点击事件
        table.on('tool(data-table)', function (obj) {
            var data = obj.data;
            var layEvent = obj.event;

            if(window.pageExt.list.beforeRowOperationEvent) {
                var doNext=window.pageExt.list.beforeRowOperationEvent(data,obj);
                if(!doNext) return;
            }

            admin.putTempData('hr-salary-form-data-form-action', "",true);
            if (layEvent === 'edit') { // 修改
                admin.post(getByIdURL, { id : data.id }, function (data) {
                    if(data.success) {
                        admin.putTempData('hr-salary-form-data-form-action', "edit",true);
                        showEditForm(data.data);
                    } else {
                        fox.showMessage(data);
                    }
                });
            } else if (layEvent === 'view') { // 查看
                admin.post(getByIdURL, { id : data.id }, function (data) {
                    if(data.success) {
                        admin.putTempData('hr-salary-form-data-form-action', "view",true);
                        showEditForm(data.data);
                    } else {
                        fox.showMessage(data);
                    }
                });
            }
            else if (layEvent === 'del') { // 删除

                if(window.pageExt.list.beforeSingleDelete) {
                    var doNext=window.pageExt.list.beforeSingleDelete(data);
                    if(!doNext) return;
                }

                top.layer.confirm(fox.translate('确定删除此'+'人员薪酬'+'吗？'), function (i) {
                    top.layer.close(i);
                    admin.post(deleteURL, { id : data.id }, function (data) {
                        top.layer.closeAll('loading');
                        if (data.success) {
                            if(window.pageExt.list.afterSingleDelete) {
                                var doNext=window.pageExt.list.afterSingleDelete(data);
                                if(!doNext) return;
                            }
                            fox.showMessage(data);
                            refreshTableData();
                        } else {
                            fox.showMessage(data);
                        }
                    },{delayLoading:100, elms:[$(".ops-delete-button[data-id='"+data.id+"']")]});
                });
            }

        });

    };

    /**
     * 打开编辑窗口
     */
    function showEditForm(data) {
        if(window.pageExt.list.beforeEdit) {
            var doNext=window.pageExt.list.beforeEdit(data);
            if(!doNext) return;
        }
        var action=admin.getTempData('hr-salary-form-data-form-action');
        var queryString="";
        if(data && data.id) queryString='id=' + data.id;
        if(window.pageExt.list.makeFormQueryString) {
            queryString=window.pageExt.list.makeFormQueryString(data,queryString,action);
        }
        admin.putTempData('hr-salary-form-data', data);
        var area=admin.getTempData('hr-salary-form-area');
        var height= (area && area.height) ? area.height : ($(window).height()*0.6);
        var top= (area && area.top) ? area.top : (($(window).height()-height)/2);
        var title = fox.translate('人员薪酬');
        if(action=="create") title=fox.translate('添加','','cmp:table')+title;
        else if(action=="edit") title=fox.translate('修改','','cmp:table')+title;
        else if(action=="view") title=fox.translate('查看','','cmp:table')+title;

        admin.popupCenter({
            title: title,
            resize: false,
            offset: [top,null],
            area: ["80%",height+"px"],
            type: 2,
            id:"hr-salary-form-data-win",
            content: '/business/hr/salary/salary_form.html' + (queryString?("?"+queryString):""),
            finish: function () {
                if(action=="create") {
                    refreshTableData();
                }
                if(action=="edit") {
                    false?refreshTableData():refreshRowData(data,true);
                }
            }
        });
    };

    window.module={
        refreshTableData: refreshTableData,
        refreshRowData: refreshRowData,
        getCheckedList: getCheckedList,
        showEditForm: showEditForm
    };

    window.pageExt.list.ending && window.pageExt.list.ending();

};


layui.use(['form', 'table', 'util', 'settings', 'admin', 'upload','foxnic','xmSelect','laydate','dropdown'],function() {
    var task=setInterval(function (){
        if(!window["pageExt"]) return;
        clearInterval(task);
        (new ListPage()).init(layui);
    },1);
});