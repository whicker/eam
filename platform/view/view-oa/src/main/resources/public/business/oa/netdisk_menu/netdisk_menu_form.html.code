<!--
/**
 * 文件菜单 列表页 JS 脚本
 * @author 金杰 , maillank@qq.com
 * @since 2023-09-18 14:20:14
 */
 -->
 <!DOCTYPE html>
<html style="background-color: #FFFFFF;">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8"/>
    <meta name="referrer" content="no-referrer">
	<title th:text="${lang.translate('文件菜单')}">文件菜单</title>
    <link th:if(theme.ico!="null") rel="shortcut icon" th:href="${theme.ico}" type="image/vnd.microsoft.icon">
    <link th:if(theme.ico!="null") rel="icon" th:href="${theme.ico}" type="image/vnd.microsoft.icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/assets/libs/layui/css/layui.css" th:href="'/assets/libs/layui/css/layui.css?'+${cacheKey}"/>
    <link rel="stylesheet" href="/assets/css/admin.css" th:href="'/assets/css/admin.css?'+${cacheKey}"/>
    <link rel="stylesheet" href="/assets/libs/toast/css/toast.css" type="text/css" th:href="'/assets/libs/toast/css/toast.css?'+${cacheKey}">
    <link rel="stylesheet" href="/assets/css/foxnic-web.css" th:href="'/assets/css/foxnic-web.css?'+${cacheKey}"/>
    <link href="/assets/libs/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="/assets/libs/material-design-webfont/css/material-design-icons-min.css" rel="stylesheet">
    <script th:inline="javascript">var foxnic_cachekey=[[${cacheKey}]];</script>
    <style>
    </style>
    <link th:each="css:${theme.css}" rel="stylesheet" th:href="${css}+'?'+${cacheKey}">
</head>

<body style="overflow-y: hidden;">
<div class="form-container" >

    <form id="data-form" lay-filter="data-form" class="layui-form model-form" style="opacity:0">

        <input name="id" id="id"  type="hidden"/>

         <!--开始：group 循环-->


        <div class="layui-row form-row" id="random-9785-content">

             <!--开始：column 循环-->
            <!-- 只有当非第一个分组没有title时才使 padding-top 为 0 -->
            <div class="layui-col-xs12 form-column" >

                <!-- text_input : 编码 ,  code -->
                <div class="layui-form-item" inlines=""  inline-delta="0" input-width="">
                    <div class="layui-form-label layui-form-label-c1"><div th:text="${lang.translate('编码')}">编码</div><div class="layui-required">*</div></div>
                    <div class="layui-input-block layui-input-block-c1">
                        <input  readonly lay-filter="code" id="code" name="code" th:placeholder="${ lang.translate('请输入'+'编码') }" type="text" class="layui-input"    lay-verify="|required"  />
                    </div>
                </div>
            
                <!-- text_input : 名称 ,  name -->
                <div class="layui-form-item" inlines=""  inline-delta="0" input-width="">
                    <div class="layui-form-label layui-form-label-c1"><div th:text="${lang.translate('名称')}">名称</div><div class="layui-required">*</div></div>
                    <div class="layui-input-block layui-input-block-c1">
                        <input lay-filter="name" id="name" name="name" th:placeholder="${ lang.translate('请输入'+'名称') }" type="text" class="layui-input"    lay-verify="|required"  />
                    </div>
                </div>
            
                <!-- radio_box : 是否显示 ,  showValue  -->
                <div class="layui-form-item" inlines=""  inline-delta="0" input-width="">
                    <div class="layui-form-label layui-form-label-c1"><div th:text="${lang.translate('是否显示')}">是否显示</div><div class="layui-required">*</div></div>
                    <div class="layui-input-block layui-input-block-c1">
                        <input input-type="radio" type="radio" name="showValue" lay-filter="showValue" th:each="e,stat:${enum.toArray('com.dt.platform.constants.enums.oa.NetDiskMenuShowEnum')}" th:value="${e.code}" th:title="${e.text}" th:checked="${(e.code=='' || stat.index==0)}">
                    </div>
                </div>
            
                <!-- text_input : 图标 ,  iconCode -->
                <div class="layui-form-item" inlines=""  inline-delta="0" input-width="">
                    <div class="layui-form-label layui-form-label-c1"><div th:text="${lang.translate('图标')}">图标</div></div>
                    <div class="layui-input-block layui-input-block-c1">
                        <input lay-filter="iconCode" id="iconCode" name="iconCode" th:placeholder="${ lang.translate('请输入'+'图标') }" type="text" class="layui-input"  />
                    </div>
                </div>
            
                <!-- radio_box : 类型 ,  type  -->
                <div class="layui-form-item" inlines=""  inline-delta="0" input-width="">
                    <div class="layui-form-label layui-form-label-c1"><div th:text="${lang.translate('类型')}">类型</div><div class="layui-required">*</div></div>
                    <div class="layui-input-block layui-input-block-c1">
                        <input input-type="radio" type="radio" name="type" lay-filter="type" th:each="e,stat:${enum.toArray('com.dt.platform.constants.enums.oa.NetDiskMenuTypeEnum')}" th:value="${e.code}" th:title="${e.text}" th:checked="${(e.code=='' || stat.index==0)}">
                    </div>
                </div>
            
                <!-- number_input : 排序 ,  sortValue  -->
                <div class="layui-form-item" inlines=""  inline-delta="0" input-width="">
                    <div class="layui-form-label layui-form-label-c1"><div th:text="${lang.translate('排序')}">排序</div><div class="layui-required">*</div></div>
                    <div class="layui-input-block layui-input-block-c1">
                        <input lay-filter="sortValue" id="sortValue" name="sortValue" th:placeholder="${ lang.translate('请输入'+'排序') }" type="text" class="layui-input"    lay-verify="|required"   autocomplete="off" input-type="number_input" integer="true" decimal="false" allow-negative="true" step="1.0"   scale="0" />
                    </div>
                </div>
            <!--结束：栏次内字段循环-->
            </div>
            <!--结束：栏次输入框循环-->
        </div>
        <!--结束：group循环-->

        <div style="height: 8px"></div>
        <div style="height: 50px"></div>


    </form>

</div>
<div class="model-form-footer">
    <button class="layui-btn layui-btn-primary" id="cancel-button" lay-filter="cancel-button" type="button" th:text="${lang.translate('取消','','form.button')}" >取消</button>
    <button th:if="${perm.checkAnyAuth('oa_netdisk_menu:create','oa_netdisk_menu:update','oa_netdisk_menu:save')}" class="layui-btn" style="margin-right: 15px;display: none;"  id="submit-button" lay-filter="submit-button" lay-submit th:text="${lang.translate('保存','','form.button')}">保存</button>
</div>

<script type="text/javascript" src="/module/global.js" th:src="'/module/global.js?'+${cacheKey}"></script>
<script type="text/javascript" src="/assets/libs/jquery-3.2.1.min.js" th:src="'/assets/libs/jquery-3.2.1.min.js?'+${cacheKey}"></script>
<script type="text/javascript" src="/assets/libs/pandyle.min.js" th:src="'/assets/libs/pandyle.min.js?'+${cacheKey}"></script>
<script type="text/javascript" src="/assets/libs/layui/layui.js" th:src="'/assets/libs/layui/layui.js?'+${cacheKey}"></script>
<script type="text/javascript" src="/assets/libs/toast/js/toast.js" th:src="'/assets/libs/toast/js/toast.js?'+${cacheKey}"></script>
<script th:inline="javascript">
    var LAYUI_TABLE_WIDTH_CONFIG = [[${layuiTableWidthConfig}]];
    var RADIO_TYPE_DATA = [[${enum.toArray('com.dt.platform.constants.enums.oa.NetDiskMenuTypeEnum')}]];
    var RADIO_SHOWVALUE_DATA = [[${enum.toArray('com.dt.platform.constants.enums.oa.NetDiskMenuShowEnum')}]];
    var VALIDATE_CONFIG={"sortValue":{"labelInForm":"排序","inputType":"number_input","required":true},"showValue":{"labelInForm":"是否显示","inputType":"radio_box","required":true},"code":{"labelInForm":"编码","inputType":"text_input","required":true},"name":{"labelInForm":"名称","inputType":"text_input","required":true},"type":{"labelInForm":"类型","inputType":"radio_box","required":true}};
    var AUTH_PREFIX="oa_netdisk_menu";


</script>



<script th:src="'/business/oa/netdisk_menu/netdisk_menu_ext.js?'+${cacheKey}"></script>
<script th:src="'/business/oa/netdisk_menu/netdisk_menu_form.js?'+${cacheKey}"></script>

</body>
</html>