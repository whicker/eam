<!--
/**
 * 我的分享 列表页 JS 脚本
 * @author 金杰 , maillank@qq.com
 * @since 2023-09-28 13:31:44
 */
 -->
 <!DOCTYPE html>
<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8"/>
    <meta name="referrer" content="no-referrer">
    <title th:text="${lang.translate('我的分享')}">我的分享</title>
    <link th:if(theme.ico!="null") rel="shortcut icon" th:href="${theme.ico}" type="image/vnd.microsoft.icon">
    <link th:if(theme.ico!="null") rel="icon" th:href="${theme.ico}" type="image/vnd.microsoft.icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/assets/libs/layui/css/layui.css" th:href="'/assets/libs/layui/css/layui.css?'+${cacheKey}"/>
    <link rel="stylesheet" href="/assets/css/admin.css" th:href="'/assets/css/admin.css?'+${cacheKey}"/>
    <link rel="stylesheet" href="/assets/libs/toast/css/toast.css" type="text/css" th:href="'/assets/libs/toast/css/toast.css?'+${cacheKey}">
    <link rel="stylesheet" href="/assets/css/foxnic-web.css" th:href="'/assets/css/foxnic-web.css?'+${cacheKey}">
    <link href="/assets/libs/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="/assets/libs/material-design-webfont/css/material-design-icons-min.css" rel="stylesheet">
    <script th:inline="javascript">var foxnic_cachekey=[[${cacheKey}]];</script>
    <style>
    </style>
    <link th:each="css:${theme.css}" rel="stylesheet" th:href="${css}+'?'+${cacheKey}">
</head>

<body style="overflow-y: hidden">

<div class="layui-card">

    <div class="layui-card-body" style="">

        <div class="search-bar" style=" display: none; ">

            <div class="search-input-rows" style="opacity: 0">
                <!-- 搜索输入区域 -->
                <div class="layui-form toolbar search-inputs">
                    <!-- 主键 , id ,typeName=text_input, isHideInSearch=true -->
                    <!-- 用户 , userId ,typeName=button, isHideInSearch=false -->
                    <div class="search-unit">
                        <div class="search-label-div" style="width:100px"><span th:text="${lang.translate('用户')}" class="search-label userId-label">用户</span><span class="search-colon">:</span></div>
                            <input lay-filter="userId" id="userId" name="userId"  type="hidden" class="layui-input"   />
                            <button id="userId-button" type="button" action-type="emp-dialog" class="layui-btn layui-btn-primary   " style="width: 180px"> <i class='layui-icon layui-icon-search'></i> <span th:text="${lang.translate('请选择人员')}" th:default-label="${lang.translate('请选择人员')}">按钮文本</span></button>
                    </div>
                    <!-- 文件 , fileId ,typeName=text_input, isHideInSearch=false -->
                    <div class="search-unit">
                        <div class="search-label-div" style="width:100px"><span th:text="${lang.translate('文件')}" class="search-label fileId-label">文件</span><span class="search-colon">:</span></div>
                        <input id="fileId" class="layui-input search-input" style="width: 180px" type="text" />
                    </div>
                    <!-- 链接 , fileUrl ,typeName=text_input, isHideInSearch=false -->
                    <div class="search-unit">
                        <div class="search-label-div" style="width:100px"><span th:text="${lang.translate('链接')}" class="search-label fileUrl-label">链接</span><span class="search-colon">:</span></div>
                        <input id="fileUrl" class="layui-input search-input" style="width: 180px" type="text" />
                    </div>
                    <!-- 文件 , fdId ,typeName=text_input, isHideInSearch=false -->
                    <div class="search-unit">
                        <div class="search-label-div" style="width:100px"><span th:text="${lang.translate('文件')}" class="search-label fdId-label">文件</span><span class="search-colon">:</span></div>
                        <input id="fdId" class="layui-input search-input" style="width: 180px" type="text" />
                    </div>
                    <!-- 文件类型 , fdType ,typeName=text_input, isHideInSearch=false -->
                    <div class="search-unit">
                        <div class="search-label-div" style="width:100px"><span th:text="${lang.translate('文件类型')}" class="search-label fdType-label">文件类型</span><span class="search-colon">:</span></div>
                        <input id="fdType" class="layui-input search-input" style="width: 180px" type="text" />
                    </div>
                    <!-- 过期时间 , expirationTime ,typeName=date_input, isHideInSearch=false -->
                    <div class="search-unit">
                        <div class="search-label-div" style="width:100px"><span th:text="${lang.translate('过期时间')}" class="search-label expirationTime-label">过期时间</span><span class="search-colon">:</span></div>
                            <input type="text" id="expirationTime"  style="width: 180px"  lay-verify="date" th:placeholder="${lang.translate('请选择')}" autocomplete="off" class="layui-input search-input search-date-input" readonly>
                    </div>
                    <!-- 过期方式 , expirationMethod ,typeName=radio_box, isHideInSearch=false -->
                    <div class="search-unit">
                        <div class="search-label-div" style="width:100px"><span th:text="${lang.translate('过期方式')}" class="search-label expirationMethod-label">过期方式</span><span class="search-colon">:</span></div>


                        <div id="expirationMethod" th:data="${enum.toArray('com.dt.platform.constants.enums.oa.NetDiskShareExpirationMethodEnum')}" style="width:180px"></div>
                    </div>
                    <!-- 类型 , type ,typeName=radio_box, isHideInSearch=false -->
                    <div class="search-unit">
                        <div class="search-label-div" style="width:100px"><span th:text="${lang.translate('类型')}" class="search-label type-label">类型</span><span class="search-colon">:</span></div>


                        <div id="type" th:data="${enum.toArray('com.dt.platform.constants.enums.oa.NetDiskShareTypeEnum')}" style="width:180px"></div>
                    </div>
                    <!-- 状态 , status ,typeName=radio_box, isHideInSearch=false -->
                    <div class="search-unit">
                        <div class="search-label-div" style="width:100px"><span th:text="${lang.translate('状态')}" class="search-label status-label">状态</span><span class="search-colon">:</span></div>


                        <div id="status" th:data="${enum.toArray('com.dt.platform.constants.enums.oa.NetDiskShareStatusEnum')}" style="width:180px"></div>
                    </div>


                </div>
            </div>


            <!-- 按钮区域 -->
            <div id="search-area" class="layui-form toolbar search-buttons" style="opacity: 0">
                <button id="search-button" class="layui-btn icon-btn"><i class="layui-icon">&#xe615;</i>&nbsp;&nbsp;<span th:text="${lang.translate('搜索','','cmp:table.search')}">搜索</span></button>
            </div>
        </div>

        <div id="table-area" style="margin-top: 0px ">
            <table class="layui-table" id="data-table" lay-filter="data-table"></table>
        </div>

    </div>
</div>

<script type="text/javascript" src="/module/global.js" th:src="'/module/global.js?'+${cacheKey}"></script>
<script type="text/javascript" src="/assets/libs/jquery-3.2.1.min.js" th:src="'/assets/libs/jquery-3.2.1.min.js?'+${cacheKey}"></script>
<script type="text/javascript" src="/assets/libs/pandyle.min.js" th:src="'/assets/libs/pandyle.min.js?'+${cacheKey}"></script>
<script type="text/javascript" src="/assets/libs/layui/layui.js" th:src="'/assets/libs/layui/layui.js?'+${cacheKey}"></script>
<script type="text/javascript" src="/assets/libs/toast/js/toast.js" th:src="'/assets/libs/toast/js/toast.js?'+${cacheKey}"></script>
<!-- 表格工具栏 -->
<script type="text/html" id="toolbarTemplate">
    <div class="layui-btn-container">
    </div>
</script>

<!-- 表格操作列 -->
<script type="text/html" id="tableOperationTemplate">





</script>


<script th:inline="javascript">
    var LAYUI_TABLE_WIDTH_CONFIG = [[${pageHelper.getTableColumnWidthConfig('data-table')}]];
    var RADIO_EXPIRATIONMETHOD_DATA = [[${enum.toArray('com.dt.platform.constants.enums.oa.NetDiskShareExpirationMethodEnum')}]];
    var RADIO_TYPE_DATA = [[${enum.toArray('com.dt.platform.constants.enums.oa.NetDiskShareTypeEnum')}]];
    var RADIO_STATUS_DATA = [[${enum.toArray('com.dt.platform.constants.enums.oa.NetDiskShareStatusEnum')}]];
    var AUTH_PREFIX="oa_netdisk_my_share";


</script>

<script th:src="'/business/oa/netdisk_my_share/netdisk_my_share_ext.js?'+${cacheKey}"></script>
<script th:src="'/business/oa/netdisk_my_share/netdisk_my_share_list.js?'+${cacheKey}"></script>

</body>
</html>